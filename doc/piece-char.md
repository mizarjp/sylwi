* Local Font Test (needs Adobe Flash)
  * http://www.fileformat.info/info/unicode/font/fontlist.htm?text=☗☖⛊⛉▲△▼▽▴▵▾▿
* Triangles
  * ▲ U+25B2 &#x25B2; &#9650; BLACK UP-POINTING TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25B2/
  * △ U+25B3 &#x25B3; &#9651; WHITE UP-POINTING TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25B3/
  * ▼ U+25BC &#x25BC; &#9660; BLACK DOWN-POINTING TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25BC/
  * ▽ U+25BD &#x25BD; &#9661; WHITE DOWN-POINTING TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25BD/
  * ▴ U+25B4 &#x25B4; &#9652; BLACK UP-POINTING SMALL TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25B4/
  * ▵ U+25B5 &#x25B5; &#9653; WHITE UP-POINTING SMALL TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25B5/
  * ▾ U+25BE &#x25BE; &#9662; BLACK DOWN-POINTING SMALL TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25BE/
  * ▿ U+25BF &#x25BF; &#9663; WHITE DOWN-POINTING SMALL TRIANGLE
    * http://www.fileformat.info/info/unicode/char/25BF/
* Unicode3.2(2002)
  * ☗ U+2617 &#x2617; &#9751; BLACK SHOGI PIECE
    * http://www.fileformat.info/info/unicode/char/2617/
  * ☖ U+2616 &#x2616; &#9750; WHITE SHOGI PIECE
    * http://www.fileformat.info/info/unicode/char/2616/
* Unicode5.2(2009)
  * ⛊ U+26CA &#x26CA; &#9930; TURNED BLACK SHOGI PIECE
    * http://www.fileformat.info/info/unicode/char/26CA/
  * ⛉ U+26C9 &#x26C9; &#9929; TURNED WHITE SHOGI PIECE
    * http://www.fileformat.info/info/unicode/char/26C9/
