# 将棋エンジン評価値グラフ用表示補正関数

±1000程度の範囲を主に映しつつ、±100000程度の評価値変動まで示せるように補正する関数。

```
f:(-∞,+∞)→(-1,+1); f(x) = (2/π)*arcsin((2/π)*arctan(x/495.543));
x0 = 1000, y0 = 0.5, a = x0/tan((π/2)*sin((π/2)*y0)) ≒ 495.543;
```

参考グラフ: https://www.desmos.com/calculator/mezsf0sr6u

以前、ShogiGUI/ShogiDroid向けに非線形な縦軸目盛として提案したもののパラメータ変更版。

http://siganus.php.xdomain.jp/phpBB3/viewtopic.php?f=6&t=326
