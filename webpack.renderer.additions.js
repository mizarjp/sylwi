const webpackRenderer = require('electron-webpack/webpack.renderer.config.js');
const { inspect } = require('util');
const inProduction = process.env.NODE_ENV === 'production';

webpackRenderer().then(config => {
  config.module.rules.unshift(
    {
      test: /\.ts$/,
      enforce: 'pre',
      loader: 'tslint-loader',
      options: {},
    },
  );
  config.module.rules
  .filter((v) => '.vue'.match(v.test))
  .forEach((v) => { v.use.options.loaders.i18n = '@kazupon/vue-i18n-loader'; });
  console.log(inspect(config, {
    showHidden: false,
    depth: null,
    colors: true
  }));
});
