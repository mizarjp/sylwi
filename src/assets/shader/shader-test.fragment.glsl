precision highp float;

varying vec2 vUV;

uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform float iTime;
uniform vec2 iResolution;

#define mat4_rotx_sc(s, c) mat4(1., 0., 0., 0., 0., c, s, 0., 0., -(s), c, 0., 0., 0., 0., 1.)
#define mat4_roty_sc(s, c) mat4(c, 0., -(s), 0., 0., 1., 0., 0., s, 0., c, 0., 0., 0., 0., 1.)
#define mat4_rotz_sc(s, c) mat4(c, s, 0., 0., -(s), c, 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.)
#define mat4_mul_add(s, a) mat4(s.x, 0., 0., 0., 0., s.y, 0., 0., 0., 0., s.z, 0., a.x, a.y, a.z, 1.)

// sholder angle
const float angle_a = radians(18.);
// side angle
const float angle_b = radians(81.);
// surface angle
const float angle_c = radians(85.);

const float sin_a = sin(angle_a);
const float cos_a = cos(angle_a);
const float sin_b = sin(angle_b);
const float cos_b = cos(angle_b);
const float cot_b = cos_b / sin_b;
const float sin_c = sin(angle_c);
const float cos_c = cos(angle_c);
const float cot_c = cos_c / sin_c;

const vec3 vec3_zero = vec3(0., 0., 0.);
const vec3 vec3_one = vec3(1., 1., 1.);

float sdPiece_simple(vec3 p, vec3 g) {
  // Bottom
  float bo = -(p.y + g.y);
  // Shoulder
  float sh = abs(p.x) * sin_a + (p.y - g.y) * cos_a;
  // Side
  float si = (abs(p.x) - g.x) * sin_b + (p.y + g.y) * cos_b;
  // Surface
  float su = (abs(p.z) - g.z) * sin_c + (p.y + g.y) * cos_c;

  return max(max(bo, sh), max(si, su));
}

vec2 uv_trans(vec2 v) {
  return v * .5 + .03125;
}

// texture height multiplyer
const float heightMul = .02;
const float heightMul_0 = max(heightMul, 0.);
const float heightMul_1 = max(abs(heightMul * 2.), .01);
vec4 sdPiece(vec3 p, vec3 g) {
  // translate matrix
  const mat4 mat4_bo  = mat4_mul_add(vec3(.5, .5, 1.), vec3(4./8., 1./8., 0.)) * mat4_rotx_sc(-1., +0.);
  const mat4 mat4_shl = mat4_mul_add(vec3(.5, .5, 1.), vec3(4./8., 3./8., 0.)) * mat4_rotx_sc(+1., +0.) * mat4_rotz_sc(-sin_a, +cos_a);
  const mat4 mat4_shr = mat4_mul_add(vec3(.5, .5, 1.), vec3(4./8., 3./8., 0.)) * mat4_rotx_sc(+1., +0.) * mat4_rotz_sc(+sin_a, +cos_a);
  const mat4 mat4_sil = mat4_mul_add(vec3(.5, .5, 1.), vec3(1./8., 2./8., 0.)) * mat4_roty_sc(+1., +0.) * mat4_rotz_sc(+cos_b, +sin_b);
  const mat4 mat4_sir = mat4_mul_add(vec3(.5, .5, 1.), vec3(7./8., 2./8., 0.)) * mat4_roty_sc(-1., +0.) * mat4_rotz_sc(-cos_b, +sin_b);
  const mat4 mat4_su0 = mat4_mul_add(vec3(.5, .5, 1.), vec3(2./8., 6./8., 0.)) * mat4_roty_sc(+0., +1.) * mat4_rotx_sc(+cos_c, +sin_c);
  const mat4 mat4_su1 = mat4_mul_add(vec3(.5, .5, 1.), vec3(6./8., 6./8., 0.)) * mat4_roty_sc(+0., -1.) * mat4_rotx_sc(-cos_c, +sin_c);
  // texture border
  const vec2 bo_lo  = vec2(1./4., 0./4.);
  const vec2 bo_hi  = vec2(3./4., 1./4.);
  const vec2 shl_lo = vec2(1./4., 1./4.);
  const vec2 shl_hi = vec2(2./4., 2./4.);
  const vec2 shr_lo = vec2(2./4., 1./4.);
  const vec2 shr_hi = vec2(3./4., 2./4.);
  const vec2 sil_lo = vec2(0./4., 0./4.);
  const vec2 sil_hi = vec2(1./4., 2./4.);
  const vec2 sir_lo = vec2(3./4., 0./4.);
  const vec2 sir_hi = vec2(4./4., 2./4.);
  const vec2 su0_lo = vec2(0./4., 2./4.);
  const vec2 su0_hi = vec2(2./4., 4./4.);
  const vec2 su1_lo = vec2(2./4., 2./4.);
  const vec2 su1_hi = vec2(4./4., 4./4.);
  // boundbox cut
  const vec3 boundLimen = vec3_zero + heightMul_0 + .125;
  vec3 bound = abs(p) - g;
  if (any(greaterThan(bound, boundLimen))) return vec4(0., 0., 0., length(max(bound - heightMul_0, 0.)));

  // pos coord negative/positive boolvector
  bvec3 posp = greaterThanEqual(p, vec3_zero);

  // Bottom
  vec4 bo = mat4_bo * vec4(p + vec3(0., g.y, 0.), 1.);
  vec2 bo_xy = clamp(bo.xy, bo_lo, bo_hi);
  float bo_z = bo.z + texture2D(iChannel1, uv_trans(bo_xy)).x * -heightMul;
  // Shoulder
  vec4 sh = posp.x ?
    mat4_shr * vec4(p + vec3(0., -g.y, 0.), 1.):
    mat4_shl * vec4(p + vec3(0., -g.y, 0.), 1.);
  vec2 sh_xy = posp.x ? clamp(sh.xy, shr_lo, shr_hi) : clamp(sh.xy, shl_lo, shl_hi);
  float sh_z = sh.z + texture2D(iChannel1, uv_trans(sh_xy)).x * -heightMul;
  // Side
  vec4 si = posp.x ?
    mat4_sir * vec4(p + vec3(+g.y * cot_b - g.x, 0., 0.), 1.):
    mat4_sil * vec4(p + vec3(-g.y * cot_b + g.x, 0., 0.), 1.);
  vec2 si_xy = posp.x ? clamp(si.xy, sir_lo, sir_hi) : clamp(si.xy, sil_lo, sil_hi);
  float si_z = si.z + texture2D(iChannel1, uv_trans(si_xy)).x * -heightMul;
  // Surface
  vec4 su = posp.z ?
    mat4_su0 * vec4(p + vec3(0., 0., +g.y * cot_c - g.z), 1.):
    mat4_su1 * vec4(p + vec3(0., 0., -g.y * cot_c + g.z), 1.);
  vec2 su_xy = posp.z ? clamp(su.xy, su0_lo, su0_hi) : clamp(su.xy, su1_lo, su1_hi);
  float su_z = su.z + texture2D(iChannel1, uv_trans(su_xy)).x * -heightMul;

  float max_z = max(max(bo_z, sh_z), max(si_z, su_z));
  if (max_z == bo_z) return vec4(texture2D(iChannel0, bo_xy).xyz, max_z);
  if (max_z == sh_z) return vec4(texture2D(iChannel0, sh_xy).xyz, max_z);
  if (max_z == si_z) return vec4(texture2D(iChannel0, si_xy).xyz, max_z);
  return vec4(texture2D(iChannel0, su_xy).xyz, max_z);

}

vec4 distanceHub(vec3 p) {
  // position
  const vec3 p_k  = vec3(+1., -.5, 0.); // King
  const vec3 p_rb = vec3( 0., -.5, 0.); // Rook, Bishop
  const vec3 p_gs = vec3(-1., -.5, 0.); // Gold, Silver
  const vec3 p_n  = vec3(+1., +.5, 0.); // kNight
  const vec3 p_l  = vec3( 0., +.5, 0.); // Lance
  const vec3 p_p  = vec3(-1., +.5, 0.); // Pawn
  // half height/width/thick (square height = 1.)
  const vec3 g_k  = vec3(.371, .413, .125); // King
  const vec3 g_rb = vec3(.358, .400, .120); // Rook, Bishop
  const vec3 g_gs = vec3(.345, .387, .114); // Gold, Silver
  const vec3 g_n  = vec3(.329, .374, .107); // kNight
  const vec3 g_l  = vec3(.303, .362, .103); // Lance
  const vec3 g_p  = vec3(.291, .349, .100); // Pawn
  // rotate
  float cos_x = cos(iTime * 0.4);
  float sin_x = sin(iTime * 0.4);
  float cos_z = cos(iTime * 1.0);
  float sin_z = sin(iTime * 1.0);
  // rotate matrix
  mat3 m =
    mat3(cos_z, -sin_z, 0., sin_z, cos_z, 0., 0., 0., 1.) *
    mat3(1., 0., 0., 0., cos_x, sin_x, 0., -sin_x, cos_x);
  // distance
  vec4 d[6] = vec4[](
    sdPiece(m * (p + p_k ), g_k ), // King
    sdPiece(m * (p + p_rb), g_rb), // Rook, Bishop
    sdPiece(m * (p + p_gs), g_gs), // Gold, Silver
    sdPiece(m * (p + p_n ), g_n ), // kNight
    sdPiece(m * (p + p_l ), g_l ), // Lance
    sdPiece(m * (p + p_p ), g_p )  // Pawn
  );
  vec4 d_min = d[0];

  for (int i = 1; i < 6; i++) {
    if (d_min.w > d[i].w) {
      d_min = d[i];
    }
  }

  return d_min;
}

vec3 genNormal(vec3 p) {
  const float d = .001;
  const vec3 xp = vec3(+d, 0., 0.);
  const vec3 yp = vec3(0., +d, 0.);
  const vec3 zp = vec3(0., 0., +d);
  const vec3 xn = vec3(-d, 0., 0.);
  const vec3 yn = vec3(0., -d, 0.);
  const vec3 zn = vec3(0., 0., -d);
  return normalize(vec3(
    distanceHub(p + xp).w - distanceHub(p + xn).w,
    distanceHub(p + yp).w - distanceHub(p + yn).w,
    distanceHub(p + zp).w - distanceHub(p + zn).w
  ));
}

void mainImage(out vec4 fragColor, in vec2 fragCoord) {
  const vec3 light = normalize(vec3(-.5, .5, 1.));
  vec2 p = (fragCoord.xy - iResolution.xy * .5) / min(iResolution.x, iResolution.y*1.33);
  vec3 cPos = vec3(0., 0.,  3.2);
  vec3 cDir = vec3(0., 0., -1.0);
  vec3 cUp  = vec3(0., 1.,  0.0);
  vec3 cSide = cross(cDir, cUp);
  float targetDepth = 1.;
  vec3 ray = normalize(cSide * p.x + cUp * p.y + cDir * targetDepth);
  vec4 dist;
  float rLen = 0.;
  float rLenPrev = 0.;
  vec3 rPos = cPos;
  for (int i = 0; i < 1; ++i) {
    dist = distanceHub(rPos);
    rLen += dist.w;
    rPos = cPos + ray * rLen;
  }
  for (int i = 0; i < 256; ++i) {
    vec4 prevDist = dist;
    dist = distanceHub(rPos);
    float d = abs(dist.w);
    if (d > 5. || d < .001) { break; }
    if (prevDist.w * dist.w < 0.) {
      float lb = rLenPrev;
      float hb = rLen;
      rLen = rLen - .5 * prevDist.w;
      rPos = cPos + ray * rLen;
      for (i = 0; i < 64; ++i) {
        dist = distanceHub(rPos);
        if (abs(dist.w) < .001) { break; }
        // if (abs(hb - lb) < .001) { break; }
        if (prevDist.w * dist.w < 0.) {
          hb = rLen;
        } else {
          lb = rLen;
        }
        rLen = .5 * (hb + lb);
        rPos = cPos + ray * rLen;
      }
      break;
    }
    rLenPrev = rLen;
    rLen += d < heightMul_1 ? clamp(dist.w, -.005, .005) : dist.w;
    rPos = cPos + ray * rLen;
  }
  if (abs(dist.w) < .001) {
    vec3 normal = genNormal(rPos);
    float diff = max(dot(normal, light), .2);
    fragColor = vec4(dist.xyz * diff, 1.);
  } else {
    fragColor = vec4(.2, .6, .3, 1.);
  }
}

void main(void) {
  vec4 fragColor;
  mainImage(fragColor, vUV.xy * iResolution.xy);
  gl_FragColor = fragColor;
}
