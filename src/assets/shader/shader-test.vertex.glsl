precision highp float;

attribute vec3 position;
attribute vec2 uv;

varying vec2 vUV;

void main(void) {
  gl_Position = vec4(position.xy, 1.0, 1.0);
  vUV = uv;
}
