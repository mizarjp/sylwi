import electron from 'electron';

export default function (appStat: {
  app: Electron.App;
  mainWindow: Electron.BrowserWindow | null;
  tray: Electron.Tray | null;
  menu: Electron.Menu | null;
  createMainWindow: () => Electron.BrowserWindow;
  createTray: () => Electron.Tray;
  createMenu: (aStat: typeof appStat) => Electron.Menu;
}) {
  const templateMenu: electron.MenuItemConstructorOptions[] = [
    {
      label: 'File',
      submenu: [
        {
          label: 'Home',
          click(menuItem: electron.MenuItem, browserWindow: electron.BrowserWindow, event: electron.Event) {
            browserWindow.loadURL(
              process.env.NODE_ENV !== 'production' ?
                `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}` :
                `file://${__dirname}/index.html`,
            );
          },
        },
        { label: 'New', enabled: false },
        { label: 'Load Game', enabled: false },
        { label: 'Save Game', enabled: false },
        { label: 'Save Game As', enabled: false },
        { label: 'Save Position', enabled: false },
        { label: 'Import game from Web', enabled: false },
        { label: 'Save board image to file', enabled: false },
        { label: 'Save graph image to file', enabled: false },
        { role: 'close' },
        { role: 'quit' },
      ],
    },
    {
      label: 'Edit',
      submenu: [
        { role: 'undo' },
        { role: 'redo' },
        { type: 'separator' },
        { label: 'Input cancellation', enabled: false },
        { label: 'Pass', enabled: false },
        { type: 'separator' },
        {
          label: 'Copy notation to clipboard',
          submenu: [
            { label: 'KIF', enabled: false },
            { label: 'KI2', enabled: false },
            { label: 'CSA', enabled: false },
            { label: 'Sfen', enabled: false },
            { label: 'PSN', enabled: false },
            { label: 'PSN2', enabled: false },
          ],
          enabled: false,
        },
        {
          label: 'Copy position to clipboard',
          submenu: [
            { label: 'KIF/KI2', enabled: false },
            { label: 'CSA', enabled: false },
            { label: 'Sfen', enabled: false },
            { label: 'PSN/PSN2', enabled: false },
          ],
          enabled: false,
        },
        { label: 'Paste notation/position', enabled: false },
        { type: 'separator' },
        { label: 'Copy board image to clipboard', enabled: false },
        { label: 'Copy evaluation image to clipboard', enabled: false },
        { type: 'separator' },
        { label: 'Copy Url of Web potition diagram to clipboard', enabled: false },
        { label: 'Position display in browser', enabled: false },
        { label: 'Kyokumen pedia', enabled: false },
      ],
    },
    {
      label: 'View',
      submenu: [
        { label: 'Turn board', enabled: false },
        { type: 'separator' },
        { type: 'checkbox', label: 'Thinking infomation', enabled: false },
        { type: 'checkbox', label: 'Comments', enabled: false },
        { type: 'checkbox', label: 'Tree diagrams', enabled: false },
        { type: 'checkbox', label: 'Comments(Info)', enabled: false },
        { type: 'checkbox', label: 'Kibitzer', enabled: false },
        { type: 'checkbox', label: 'Analysis list', enabled: false },
        { type: 'separator' },
        { label: '2nd board', enabled: false },
        { type: 'separator' },
        { role: 'zoomin' },
        { role: 'zoomout' },
        { role: 'resetzoom' },
        { type: 'separator' },
        { role: 'minimize' },
        { role: 'togglefullscreen' },
        {
          type: 'checkbox',
          label: 'Auto hide menu bar',
          click(menuItem: electron.MenuItem, browserWindow: electron.BrowserWindow, event: electron.Event) {
            browserWindow.setAutoHideMenuBar(menuItem.checked);
          },
        },
      ],
    },
    {
      label: 'Game',
      submenu: [
        { label: 'Start new game', enabled: false },
        { label: 'Start new P2P game', enabled: false },
        { label: 'Start new server game', enabled: false },
        { label: 'Start new tournament', enabled: false },
        { type: 'separator' },
        { type: 'separator' },
        { label: 'Analyze', enabled: false },
        { label: 'Analyze game', enabled: false },
        { label: 'Analyze game consecutively', enabled: false },
        { type: 'separator' },
        { label: 'Solve mate problem', enabled: false },
        { label: 'Solve mate problem consecutively', enabled: false },
        { type: 'separator' },
        { label: 'Resign', enabled: false },
        { label: 'Stop', enabled: false },
      ],
    },
    {
      label: 'Book',
      submenu: [
        { label: 'Open the default book', enabled: false },
        { label: 'Open the book', enabled: false },
        { type: 'separator' },
        { label: 'New', enabled: false },
        { label: 'Open', enabled: false },
        { label: 'Save', enabled: false },
        { label: 'Save As', enabled: false },
        { label: 'Close', enabled: false },
        {
          label: 'Export',
          submenu: [
            { label: 'KIF', enabled: false },
          ],
          enabled: false,
        },
        { type: 'separator' },
        { label: 'Add move to book', enabled: false },
        { label: 'Add moves from file', enabled: false },
        { label: 'Add from clipboard', enabled: false },
        { type: 'separator' },
        { label: 'Display evaluation', enabled: false },
        { label: 'Book comments', enabled: false },
        { label: 'Book tree diagrams', enabled: false },
        { label: 'Evaluation Book', enabled: false },
        { type: 'separator' },
        { label: 'Book Manage', enabled: false },
        { label: 'Book Info', enabled: false },
      ],
    },
    {
      label: 'EditPosition',
      submenu: [
        { label: 'Edit start', enabled: false },
        { label: 'Change turn', enabled: false },
        { label: 'Rotate position', enabled: false },
        { type: 'separator' },
        { label: 'Initial Position', enabled: false },
        { label: 'Initial Position (handicap)', enabled: false },
        { label: 'Mate problem Position', enabled: false },
        { type: 'separator' },
        { label: 'Mirror', enabled: false },
      ],
    },
    {
      label: 'Tools',
      submenu: [
        { label: 'Engine settings', enabled: false },
        { type: 'separator' },
        { label: 'Game Settings', enabled: false },
        { label: 'Consider Settings', enabled: false },
        { label: 'Hint Settings', enabled: false },
        { label: 'Mate Settings', enabled: false },
        { type: 'separator' },
        { label: 'Options', enabled: false },
        { type: 'separator' },
        { label: 'Open the game automatic save folder', enabled: false },
        { type: 'separator' },
        { role: 'reload' },
        {
          label: 'ReCreate Window',
          click() {
            // WebGLのContextLostを回復するにはこれだけでは足りない模様。
            const prevMainWindow = appStat.mainWindow;
            appStat.mainWindow = appStat.createMainWindow();
            appStat.mainWindow.setMenu(appStat.menu = appStat.createMenu(appStat));
            if (prevMainWindow) {
              prevMainWindow.close();
            }
          },
        },
        { role: 'toggledevtools' },
        {
          label: 'GPU status info',
          click(menuItem: electron.MenuItem, browserWindow: electron.BrowserWindow, event: electron.Event) {
            new electron.BrowserWindow({ webPreferences: { webSecurity: false } }).loadURL(`chrome://gpu`);
          },
        },
      ],
    },
    {
      label: 'Help',
      submenu: [
        { label: 'Welcome', enabled: false },
        { label: 'Release Note', enabled: false },
        { type: 'separator' },
        { label: 'Version Info', enabled: false },
      ],
    },
  ];
  return electron.Menu.buildFromTemplate(templateMenu);
}
