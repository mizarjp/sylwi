import { app, BrowserWindow, Tray } from 'electron';
import createMenu from './create-menu';
import * as path from 'path';
import webpackDefs from '../common/webpack-defs';
import { format as formatUrl } from 'url';

const isDevelopment = process.env.NODE_ENV !== 'production';

// マルチウィンドウ構成にするならもう少し工夫すべき？
const appStat: {
  app: Electron.App;
  mainWindow: Electron.BrowserWindow | null;
  tray: Electron.Tray | null;
  menu: Electron.Menu | null;
  createMainWindow: () => Electron.BrowserWindow;
  createTray: () => Electron.Tray;
  createMenu: (aStat: typeof appStat) => Electron.Menu;
} = {
  app,
  createMainWindow,
  createTray,
  createMenu,
  mainWindow: null,
  tray: null,
  menu: null,
};

function createMainWindow() {
  // Construct new BrowserWindow
  const browserWindow = new BrowserWindow();

  if (isDevelopment) {
    browserWindow.webContents.openDevTools();
  }

  if (isDevelopment) {
    // points to `webpack-dev-server` in development
    browserWindow.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`);
  } else {
    // points to `index.html` in production
    browserWindow.loadURL(formatUrl({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true,
    }));
  }

  browserWindow.on('closed', () => {
    if (appStat.mainWindow === browserWindow) {
      appStat.mainWindow = null;
    }
  });

  browserWindow.webContents.on('devtools-opened', () => {
    browserWindow.focus();
    setImmediate(() => {
      browserWindow.focus();
    });
  });

  return browserWindow;
}

function createTray() {
  const tray = new Tray(require('path').join(webpackDefs.staticDir, 'icon.png'));
  tray.setToolTip('Sylwi');
  return tray;
}

// Quit application when all windows are closed
app.on('window-all-closed', () => {
  // On macOS it is common for applications to stay open
  // until the user explicitly quits
  if (process.platform !== 'darwin') app.quit();
});

app.on('activate', () => {
  // On macOS it is common to re-create a window
  // even after all windows have been closed
  if (appStat.tray === null) appStat.tray = createTray();
  if (appStat.mainWindow === null) appStat.mainWindow = createMainWindow();
  if (appStat.menu === null) appStat.mainWindow.setMenu(appStat.menu = createMenu(appStat));
});

// Create main BrowserWindow when electron is ready
app.on('ready', () => {
  appStat.tray = createTray();
  appStat.mainWindow = createMainWindow();
  appStat.mainWindow.setMenu(appStat.menu = createMenu(appStat));
});
