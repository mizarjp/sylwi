import { Component, Prop, Vue, Watch } from 'vue-property-decorator';
// tslint:disable-next-line:import-name
import BABYLON from 'babylonjs';
import 'babylonjs-gui';
import late from '../common/late';
import * as shogi from '../common/shogi';

@Component({
  template: '<canvas class="view3d" ref="stage" style="touch-action:none;"/>',
})
export default class View3d extends Vue {
  engine: View3dEngine | null = null;

  // 変数名末尾の'!'はコンストラクタ内での初期化チェックを省くアサーション(TypeScript 2.7.1以降)
  // Strict property initialization checks in classes: https://github.com/Microsoft/TypeScript/pull/20075
  // Definite assignment assertions: https://github.com/Microsoft/TypeScript/pull/20166
  @Prop({ type: String, default: 'startpos' })
  sfen!: string;
  @Prop({ type: Number, default: 1 })
  ymul!: number;
  @Prop({ type: Number, default: 0.913 })
  xmul!: number;
  @Prop({ type: Number, default: 3.17 })
  handWidth!: number;
  @Prop({ type: String, default: 'jjkk' })
  kingType!: string;
  @Prop({ type: Array, default() { return [false, false, false, false]; } })
  handL2R!: boolean[];
  @Prop({ type: Array, default() { return [0.02, 0.02, 0.02, 0.02]; } })
  handGap!: number[];
  @Prop({ type: Boolean, default: false })
  infinityPieceBox!: boolean;
  @Prop({ type: Boolean, default: false })
  cameraControl!: boolean;
  @Prop({ type: Object, default() { return { reverse: false }; } })
  cameraPos!: { reverse: boolean };
  @Prop({ type: Number, default: 500 })
  animateDuration!: number;
  @Prop({ type: String, default: 'none' })
  soundName!: string;
  @Prop({ type: String, default: 'norm' })
  pieceGeoId!: string;
  @Prop({ type: String, default: 'darkPiece1k' })
  matidBPiece!: string;
  @Prop({ type: String, default: 'whitePiece1k' })
  matidWPiece!: string;
  @Prop({ type: String, default: 'grayboard' })
  matidBoard!: string;
  @Prop({ type: String, default: 'darkgrayboard' })
  matidBHandTable!: string;
  @Prop({ type: String, default: 'darkgrayboard' })
  matidWHandTable!: string;
  @Prop({ type: String, default: 'darkline' })
  matidLine!: string;
  @Prop({ type: Boolean, default: false })
  boolBoardInfo!: boolean;
  @Prop({ type: Boolean, default: false })
  boolMouseOverGlow!: boolean;
  @Prop({ type: Boolean, default: true })
  boolMoveGlow!: boolean;
  @Prop({ type: Boolean, default: true })
  boolCapGlow!: boolean;
  @Prop({ type: Boolean, default: true })
  boolSquareEm!: boolean;
  @Prop({ default() { return new BABYLON.Color3(0.10, 0.40, 0.40); } })
  sqEmColor!: BABYLON.Color3;
  @Prop({ default() { return BABYLON.Color3.Green(); } })
  glowColor!: BABYLON.Color3;
  @Prop({ default() { return BABYLON.Color3.Yellow(); } })
  mouseoverGlowColor!: BABYLON.Color3;

  mounted() {
    if (!BABYLON.Engine.isSupported()) {
      alert('WebGL is disabled or unavailable');
      return;
    }
    const engine = this.engine = new View3dEngine(this);
    engine.camReset();
    engine.setBoard();
    engine.attachCameraControl(this.cameraControl);
    engine.rerender();
    engine.animate();
  }

  destroyed() {
    if (this.engine) {
      this.engine.destroy();
      this.engine = null;
    }
  }

  @Watch('infinityPieceBox')
  onInfinityPieceBoxChanged(val: boolean, oldVal: boolean) {
    if (this.engine) {
      this.engine.pos.changeMaxPieceBox(val);
    }
  }
  @Watch('sfen')
  onSfenChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.pos.changeMaxPieceBox(this.infinityPieceBox);
      this.engine.pos.set(val);
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('cameraControl')
  onCameraControlChanged(val: boolean, oldVal: boolean) {
    if (this.engine) {
      this.engine.attachCameraControl(val);
    }
  }
  @Watch('cameraPos')
  onCameraPosChanged(val: any, oldVal: any) {
    if (val) {
      if (this.engine) {
        this.engine.cameraReverse = !!val.reverse;
        this.engine.camReset();
      }
    }
  }
  @Watch('kingType')
  onKingTypeChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('xmul')
  onXmulChanged(val: number, oldVal: number) {
    if (this.engine) {
      this.engine.createBase();
      this.engine.camReset();
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('ymul')
  onYmulChanged(val: number, oldVal: number) {
    if (this.engine) {
      this.engine.createBase();
      this.engine.camReset();
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('handWidth')
  onHandWidthChanged(val: number, oldVal: number) {
    if (this.engine) {
      this.engine.createBase();
      this.engine.camReset();
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('handGap')
  onHandGapChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('handL2R')
  onHandL2RChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('pieceGeoId')
  onPieceGeoIdChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('matidBPiece')
  onMatidBPieceChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.setBoard(true);
      this.engine.rerender();
    }
  }
  @Watch('matidWPiece')
  onMatidWPieceChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.setBoard(true);
      this.engine.rerender();
    }
  }
  @Watch('matidBoard')
  onMatidBoardChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.board.material = this.engine.mats[val].get();
      this.engine.rerender();
    }
  }
  @Watch('matidBHandTable')
  onMatidBHandTableChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.htableb.material = this.engine.mats[val].get();
      this.engine.rerender();
    }
  }
  @Watch('matidWHandTable')
  onMatidWHandTableChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.htablew.material = this.engine.mats[val].get();
      this.engine.rerender();
    }
  }
  @Watch('matidLine')
  onMatidLineChanged(val: string, oldVal: string) {
    if (this.engine) {
      this.engine.lines.material = this.engine.mats[val].get();
      this.engine.rerender();
    }
  }
  @Watch('boolDebugLayer')
  onBoolDebugLayerChanged(val: boolean, oldVal: any) {
    if (this.engine) {
      if (val) {
        this.engine.scene.debugLayer.show();
      } else {
        this.engine.scene.debugLayer.hide();
      }
    }
  }
  @Watch('boolBoardInfo')
  onBoolBoardInfoChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('boolMoveGlow')
  onBoolMoveGlowChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('boolCapGlow')
  onBoolCapGlowChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('boolSquareEm')
  onBoolSquareEmChanged(val: any, oldVal: any) {
    if (this.engine) {
      this.engine.setBoard();
      this.engine.rerender();
    }
  }
  @Watch('sqEmColor')
  onSqEmColorChanged(val: BABYLON.Color3, oldVal: any) {
    if (this.engine) {
      (this.engine.mats['square'].get() as BABYLON.StandardMaterial).diffuseColor = val;
    }
  }
  @Watch('soundName')
  onSoundNameChanged(val: string, oldVal: any) {
    if (this.engine) {
      this.engine.setSound(val);
    }
  }
}

class View3dEngine {
  component: View3d;
  canvas: HTMLCanvasElement;
  bjsengine: BABYLON.Engine;
  scene: BABYLON.Scene;
  pos: shogi.Position;

  // 変数名末尾の'!'はコンストラクタ内での初期化チェックを省くアサーション(TypeScript 2.7.1以降)
  // Strict property initialization checks in classes: https://github.com/Microsoft/TypeScript/pull/20075
  // Definite assignment assertions: https://github.com/Microsoft/TypeScript/pull/20166
  camera!: BABYLON.ArcRotateCamera;
  highlightLayer!: BABYLON.HighlightLayer;
  advDynTexture!: BABYLON.GUI.AdvancedDynamicTexture;
  textBlock!: BABYLON.GUI.TextBlock;
  shadowGenerator!: BABYLON.ShadowGenerator;
  board!: BABYLON.Mesh;
  htableb!: BABYLON.Mesh;
  htablew!: BABYLON.Mesh;
  htableg!: BABYLON.Mesh;
  lines!: BABYLON.Mesh;
  sound!: BABYLON.Sound;

  mats: { [index: string]: late<BABYLON.StandardMaterial> } = {};
  pieces: { [index: string]: BABYLON.Mesh } = {};
  pieceTrans: { [index: string]: TransSt } = {};
  animatablePieces: { [index: string]: BABYLON.Animatable } = {};
  moveHighlightMesh: { [index: string]: BABYLON.Mesh; } = {};
  mouseHighlightMesh: { [index: string]: BABYLON.Mesh; } = {};
  square: BABYLON.Mesh[] = [];
  rerenderFrame: number = 1;
  pointerState: PointerState = PointerState.VIEW;
  cameraReverse: boolean = false;

  constructor(v3d: View3d) {
    this.component = v3d;
    this.canvas = v3d.$refs.stage as HTMLCanvasElement;
    this.bjsengine = new BABYLON.Engine(this.canvas, true);
    this.scene = new BABYLON.Scene(this.bjsengine);
    this.pos = new shogi.Position({ infinityPieceBox: v3d.infinityPieceBox });
    this.pos.set(v3d.sfen);
    this.createMats();
    this.createBase();
  }

  /**
   * 各種マテリアルの定義
   */
  createMats() {

    // マテリアル: 盤(鈍色)
    this.mats['grayboard'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_board_gray', this.scene), {
        diffuseColor: new BABYLON.Color3(0.40, 0.42, 0.44),
        specularColor: new BABYLON.Color3(0.25, 0.27, 0.29),
      },
    ));

    // マテリアル: 盤(暗鈍色)
    this.mats['darkgrayboard'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_board_gray', this.scene), {
        diffuseColor: new BABYLON.Color3(0.30, 0.32, 0.34),
        specularColor: new BABYLON.Color3(0.25, 0.27, 0.29),
      },
    ));

    // マテリアル: 盤(木色)
    this.mats['woodboard'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_board_wood', this.scene), {
        diffuseColor: new BABYLON.Color3(0.55, 0.30, 0.15),
        specularColor: new BABYLON.Color3(0.19, 0.17, 0.15),
      },
    ));

    // マテリアル: 盤(暗木色)
    this.mats['darkwoodboard'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_board_wood', this.scene), {
        diffuseColor: new BABYLON.Color3(0.45, 0.20, 0.10),
        specularColor: new BABYLON.Color3(0.19, 0.17, 0.15),
      },
    ));

    // マテリアル: 線
    this.mats['darkline'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_line_dark', this.scene), {
        diffuseColor: new BABYLON.Color3(0.02, 0.02, 0.02),
        specularColor: new BABYLON.Color3(0.20, 0.20, 0.20),
      },
    ));

    // マテリアル: 線
    this.mats['lightline'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('mat_line_light', this.scene), {
        diffuseColor: new BABYLON.Color3(0.98, 0.98, 0.98),
        specularColor: new BABYLON.Color3(0.20, 0.20, 0.20),
      },
    ));

    // マテリアル: 升目
    this.mats['square'] = new late(() => Object.assign(
      new BABYLON.StandardMaterial('matsquare', this.scene), {
        diffuseColor: new BABYLON.Color3(0.10, 0.40, 0.40),
        specularColor: new BABYLON.Color3(0.20, 0.20, 0.20),
      },
    ));

    const pieceMat = (bjsId: string, pData: string, opt = {}) => new late(() => {
      return Object.assign(
        new BABYLON.StandardMaterial(bjsId, this.scene),
        {
          diffuseTexture: new BABYLON.Texture(
            pData, this.scene, undefined, undefined, undefined, () => { this.rerender(); },
          ),
          emissiveColor: new BABYLON.Color3(0.10, 0.10, 0.10),
          specularColor: new BABYLON.Color3(0.30, 0.30, 0.30),
        },
        opt,
      );
    });

    // マテリアル: 駒(黒)
    this.mats['blackPiece5h'] = pieceMat('matBlackPiece5h', texturePath.black5h);
    this.mats['blackPiece1k'] = pieceMat('matBlackPiece1k', texturePath.black1k);
    this.mats['blackPiece2k'] = pieceMat('matBlackPiece2k', texturePath.black2k);
    this.mats['blackPiece4k'] = pieceMat('matBlackPiece4k', texturePath.black4k);

    // マテリアル: 駒(暗)
    this.mats['darkPiece5h'] = pieceMat('matDarkPiece5h', texturePath.dark5h);
    this.mats['darkPiece1k'] = pieceMat('matDarkPiece1k', texturePath.dark1k);
    this.mats['darkPiece2k'] = pieceMat('matDarkPiece2k', texturePath.dark2k);
    this.mats['darkPiece4k'] = pieceMat('matDarkPiece4k', texturePath.dark4k);

    // マテリアル: 駒(白)
    this.mats['whitePiece5h'] = pieceMat('matWhitePiece5h', texturePath.white5h);
    this.mats['whitePiece1k'] = pieceMat('matWhitePiece1k', texturePath.white1k);
    this.mats['whitePiece2k'] = pieceMat('matWhitePiece2k', texturePath.white2k);
    this.mats['whitePiece4k'] = pieceMat('matWhitePiece4k', texturePath.white4k);

    // マテリアル: 駒(木色)
    this.mats['woodPiece5h'] = pieceMat('matWoodPiece5h', texturePath.wood5h);
    this.mats['woodPiece1k'] = pieceMat('matWoodPiece1k', texturePath.wood1k);
    this.mats['woodPiece2k'] = pieceMat('matWoodPiece2k', texturePath.wood1k);
    this.mats['woodPiece4k'] = pieceMat('matWoodPiece4k', texturePath.wood1k);

    // sound
    this.setSound(this.component.soundName);

  }

  /**
   * サウンドの設定
   * @param soundName
   */
  setSound(soundName: string) {
    if (!soundPath.hasOwnProperty(soundName)) { return; }
    if (this.sound) { this.sound.dispose(); }
    this.sound = new BABYLON.Sound(
      'sound', ((path) => {
        if (!soundPath[soundName].startsWith('data:')) { return path; }
        // dataスキームの文字列を babylonjs.Sound にそのまま与えるとこけるので、 Buffer に変換して返す
        const binStr = atob(path.split(',')[1]);
        const binLen = binStr.length;
        const buf = new Uint8Array(binLen);
        for (let i = 0; i < binLen; i += 1) { buf[i] = binStr.charCodeAt(i); }
        return buf.buffer;
      })(soundPath[soundName]),
      this.scene, () => {
        // 読み込み完了時の処理、現状は何もしない
      },
    );
  }

  /**
   * 基本オブジェクトの定義
   */
  createBase() {
    const cp = this.component;

    this.scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

    // カメラ
    this.camera = Object.assign(
      new BABYLON.ArcRotateCamera(
        'camera', -0.5 * Math.PI, 0.5 * Math.PI, 12 * cp.ymul,
        BABYLON.Vector3.Zero(), this.scene,
      ),
      {
        lowerAlphaLimit: -10 * Math.PI,
        upperAlphaLimit: +10 * Math.PI,
        lowerBetaLimit: -10 * Math.PI,
        upperBetaLimit: +10 * Math.PI,
        lowerRadiusLimit: 2,
      },
    );

    this.highlightLayer = new BABYLON.HighlightLayer('hl', this.scene);

    this.advDynTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI('sylwiUI', true, this.scene);
    this.textBlock = new BABYLON.GUI.TextBlock();
    this.advDynTexture.addControl(this.textBlock);
    this.textBlock.textHorizontalAlignment = BABYLON.GUI.TextBlock.HORIZONTAL_ALIGNMENT_RIGHT;
    this.textBlock.textVerticalAlignment = BABYLON.GUI.TextBlock.VERTICAL_ALIGNMENT_TOP;
    this.textBlock.color = 'black';
    this.textBlock.shadowBlur = 4;
    this.textBlock.shadowOffsetX = 1;
    this.textBlock.shadowOffsetY = 1;
    this.textBlock.shadowColor = 'white';
    this.textBlock.paddingBottom = 4;
    this.textBlock.paddingRight = 4;
    this.textBlock.paddingTop = 4;
    this.textBlock.paddingLeft = 4;
    this.textBlock.fontStyle = 'bold';
    this.textBlock.fontFamily = "'Fira Code', 'Noto Sans JP', serif";

    /*
    // レンズ効果、かなり描画が重くなるのでコメントアウト
    new babylonjs.LensRenderingPipeline('lensEffects', { edge_blur: 1.0 }, this.scene, 2.0, [this.camera]);
    this.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline('lensEffects', this.camera);
    */

    // 影ジェネレータ, 影投影用光源(平行光源)
    this.shadowGenerator = Object.assign(
      new BABYLON.ShadowGenerator(1024, Object.assign(
        new BABYLON.DirectionalLight('light0', new BABYLON.Vector3(1, 0, 2).normalize(), this.scene), {
          position: new BABYLON.Vector3(-8, 0, -16),
          intensity: 0.7,
        },
      )),
      {
        // useExponentialShadowMap: true,
        useBlurExponentialShadowMap: true,
        forceBackFacesOnly: true,
        // bias: 0.01,
      },
    );

    // 副光源1(半球光源)
    Object.assign(
      new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 0, -1), this.scene), {
        intensity: 0.3,
      },
    );

    // メッシュ定義: 盤
    this.board = Object.assign(
      BABYLON.MeshBuilder.CreateGround(
        'board',
        {
          width: 9 * cp.xmul + 0.5 * cp.ymul,
          height: 9.5 * cp.ymul,
        },
        this.scene),
      {
        material: this.mats[cp.matidBoard].get(),
        rotation: new BABYLON.Vector3(-0.5 * Math.PI, 0, 0),
        receiveShadows: true,
      },
    );

    // メッシュ定義: 駒台黒
    this.htableb = Object.assign(
      BABYLON.MeshBuilder.CreateGround(
        'htableb',
        {
          width: cp.handWidth,
          height: 9.5 * cp.ymul,
        },
        this.scene),
      {
        material: this.mats[cp.matidBHandTable].get(),
        position: new BABYLON.Vector3(
          4.5 * cp.xmul + 0.3 * cp.ymul + 0.5 * cp.handWidth, 0, 0.16 * cp.ymul),
        rotation: new BABYLON.Vector3(-0.5 * Math.PI, 0, 0),
        receiveShadows: true,
      },
    );

    // メッシュ定義: 駒台白
    this.htablew = Object.assign(
      BABYLON.MeshBuilder.CreateGround(
        'htablew',
        {
          width: cp.handWidth,
          height: 9.5 * cp.ymul,
        },
        this.scene),
      {
        material: this.mats[cp.matidWHandTable].get(),
        position: new BABYLON.Vector3(
          -4.5 * cp.xmul - 0.3 * cp.ymul - 0.5 * cp.handWidth, 0, 0.16 * cp.ymul),
        rotation: new BABYLON.Vector3(0.5 * Math.PI, 0, Math.PI),
        receiveShadows: true,
      },
    );

    const lines: BABYLON.Mesh[] = [];

    // メッシュ定義: 線
    for (let i = 0; i <= 9; i += 1) {
      lines.push(Object.assign(
        BABYLON.MeshBuilder.CreateBox(
          `hline${i}`, {
            width: 9 * cp.xmul + 0.02 * cp.ymul,
            height: 0.02 * cp.ymul,
            depth: 0.001 * cp.ymul,
          },
          this.scene),
        {
          position: new BABYLON.Vector3(0, (4.5 - i) * cp.ymul, 0),
        },
      ));
      lines.push(Object.assign(
        BABYLON.MeshBuilder.CreateBox(
          `vline${i}`, {
            width: 0.02 * cp.ymul,
            height: 9 * cp.ymul + 0.02 * cp.ymul,
            depth: 0.001 * cp.ymul,
          },
          this.scene),
        {
          position: new BABYLON.Vector3((4.5 - i) * cp.xmul, 0, 0),
        },
      ));
    }

    // メッシュ定義: 星目
    for (const x of [-1, 1]) {
      for (const y of [-1, 1]) {
        lines.push(Object.assign(
          BABYLON.MeshBuilder.CreateSphere(
            `star${x + 1 >> 1}${y + 1 >> 1}`,
            {
              diameterX: 0.07 * cp.ymul,
              diameterY: 0.07 * cp.ymul,
              diameterZ: 0.003 * cp.ymul,
            },
            this.scene),
          {
            position: new BABYLON.Vector3(1.5 * x * cp.xmul, 1.5 * y * cp.ymul, 0),
          },
        ));
      }
    }

    // combilne lines
    this.lines = BABYLON.Mesh.MergeMeshes(lines) || new BABYLON.Mesh('lines');
    if (this.lines) {
      this.lines.material = this.mats[cp.matidLine].get();
      this.lines.receiveShadows = true;
    }

    // メッシュ定義: 升目
    this.square = [];
    for (let sq = 0; sq < 81; sq += 1) {
      const fr = shogi.sq2fr(sq) || { f: NaN, r: NaN };
      this.square[sq] = Object.assign(
        BABYLON.MeshBuilder.CreateGround(
          `sq${fr.f}${fr.r}`, { width: cp.xmul, height: 1 }, this.scene),
        {
          material: this.mats['square'].get(),
          position: new BABYLON.Vector3(
            (4 - fr.f) * cp.xmul, (4 - fr.r) * cp.ymul, -0.0002 * cp.ymul),
          rotation: new BABYLON.Vector3(-0.5 * Math.PI, 0, 0),
          receiveShadows: true,
        },
      );
      this.square[sq].setEnabled(false);
    }
    this.square[shogi.Square.BLACK_HAND] = Object.assign(
      BABYLON.MeshBuilder.CreateGround('sqHandBlack', { width: cp.handWidth, height: 9.5 * cp.ymul }, this.scene),
      {
        material: this.mats['square'].get(),
        position: this.htableb.position.add(new BABYLON.Vector3(0, 0, -0.0002 * cp.ymul)),
        rotation: new BABYLON.Vector3(-0.5 * Math.PI, 0, 0),
        receiveShadows: true,
      });
    this.square[shogi.Square.WHITE_HAND] = Object.assign(
      BABYLON.MeshBuilder.CreateGround('sqHandWhite', { width: cp.handWidth, height: 9.5 * cp.ymul }, this.scene),
      {
        material: this.mats['square'].get(),
        position: this.htablew.position.add(new BABYLON.Vector3(0, 0, -0.0002 * cp.ymul)),
        rotation: new BABYLON.Vector3(-0.5 * Math.PI, 0, 0),
        receiveShadows: true,
      });
    this.square[shogi.Square.GRAY_HAND] = new BABYLON.Mesh('sqHandGray', this.scene);
    this.square[shogi.Square.ERROR_HAND] = new BABYLON.Mesh('sqHandError', this.scene);
    this.square[shogi.Square.GRAY_HAND].setEnabled(false);
    this.square[shogi.Square.BLACK_HAND].setEnabled(false);
    this.square[shogi.Square.WHITE_HAND].setEnabled(false);
    this.square[shogi.Square.ERROR_HAND].setEnabled(false);

  }

  /**
   * 盤上駒の配置情報設定
   * @param {TransStHash} pTrans 配置情報の格納用オブジェクト
   * @param {shogi.Piece} p 配置する駒
   */
  setPiece(pTrans: TransStHash, p: shogi.Piece): void {
    const cp = this.component;
    const pfr = shogi.sq2fr(p.square());
    if (!pfr) return;
    const p0s = p.pt0WhiteSfen();
    const pid = `${p0s}${p.seq()}`;
    const ptx = (p.pt0() === shogi.PieceType0.k) ? cp.kingType[p.initcolor()] : p0s;
    const pGeo = pieceGeo[cp.pieceGeoId][ptx];
    const rotZ = p.color() === shogi.Color.BLACK ? 0 : Math.PI;
    const rotX = pGeo.gamma;
    const rotY = p.promote() ? Math.PI : 0;
    pTrans[pid] = {
      p, pid, ptx, pGeo,
      rotZ, rotX, rotY,
      pGeoName: cp.pieceGeoId,
      pos:
        BABYLON.Vector3.TransformCoordinates(
          BABYLON.Vector3.TransformCoordinates(
            // pivot point
            new BABYLON.Vector3(0, pGeo.hheight * cp.ymul, -pGeo.hthick * cp.ymul),
            BABYLON.Matrix.RotationX(pGeo.gamma),
          ),
          BABYLON.Matrix.RotationZ(p.color() === shogi.Color.BLACK ? 0 : Math.PI),
        ).add(
          new BABYLON.Vector3(
            (4 - pfr.f) * cp.xmul,
            (4 - pfr.r + pieceGeo[cp.pieceGeoId]['k'].hfacevert *
              (p.color() === shogi.Color.BLACK ? -1 : 1)) * cp.ymul,
            0,
          ),
        ),
      rotQ:
        BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, rotZ).multiply(
          BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, rotX)).multiply(
            BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, rotY)),
    };
  }

  /**
   * 駒台への駒の配置情報設定
   * @param {TransStHash} pTrans 配置情報の格納用オブジェクト
   * @param {shogi.Piece} p 配置する駒
   * @param {shogi.Color} c 配置する駒台
   * @param {number} r 並べる駒の段
   * @param {TransHand} st 配置補助情報
   */
  setPieceHand(pTrans: TransStHash, p: shogi.Piece, c: shogi.Color, rPos: BABYLON.Vector3, st: TransHand) {
    const cp = this.component;
    const p0s = p.pt0WhiteSfen();
    const pid = `${p0s}${p.seq()}`;
    const ptx = (p.pt0() === shogi.PieceType0.k) ? cp.kingType[p.initcolor()] : p0s;
    const pGeo = pieceGeo[cp.pieceGeoId][ptx];
    const rotZ = (p.color() === shogi.Color.BLACK ? 0 : Math.PI) + st.rotZ;
    const rotX = pGeo.gamma;
    const rotY = p.promote() ? Math.PI : 0;
    pTrans[pid] = {
      p, pid, ptx, pGeo,
      rotZ, rotX, rotY,
      pGeoName: cp.pieceGeoId,
      pos:
        BABYLON.Vector3.TransformCoordinates(
          BABYLON.Vector3.TransformCoordinates(
            BABYLON.Vector3.TransformCoordinates(
              new BABYLON.Vector3(0, pGeo.hheight * cp.ymul, -pGeo.hthick),
              BABYLON.Matrix.RotationX(pGeo.gamma),
            ),
            BABYLON.Matrix.RotationZ(st.rotZ),
          ).add(st.cVec).add(rPos),
          BABYLON.Matrix.RotationZ(
            p.color() === shogi.Color.BLACK ? 0 :
            p.color() === shogi.Color.WHITE ? Math.PI : 0),
        ).add((
          c === shogi.Color.BLACK ? this.htableb :
          c === shogi.Color.WHITE ? this.htablew : this.htableg).position),
      rotQ:
        BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, rotZ).multiply(
          BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, rotX)).multiply(
            BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, rotY)),
    };
  }

  /**
   * 駒台への駒配置計算
   * @param pg 駒の配列
   * @param l2r 配置順の左右方向
   * @param gapWidth 駒の間隔
   */
  calcHandPiece(pg: shogi.Piece[], l2r: boolean, gapWidth: number) {
    const cp = this.component;
    const overall: TransHand = {
      lVec: BABYLON.Vector3.Zero(),
      cVec: BABYLON.Vector3.Zero(),
      rVec: BABYLON.Vector3.Zero(),
      rotZ: 0,
    };
    const sepres: TransHand[] = [];
    const addVec = (s: TransHand, v: BABYLON.Vector3) => {
      s.lVec.addInPlace(v);
      s.cVec.addInPlace(v);
      s.rVec.addInPlace(v);
    };
    const addVecM = (v: BABYLON.Vector3) => {
      addVec(overall, v);
      for (const e of sepres) { addVec(e, v); }
    };
    const rotZVec = (s: TransHand, rZ: number, m: BABYLON.Matrix) => {
      BABYLON.Vector3.TransformCoordinatesToRef(s.lVec, m, s.lVec);
      BABYLON.Vector3.TransformCoordinatesToRef(s.cVec, m, s.cVec);
      BABYLON.Vector3.TransformCoordinatesToRef(s.rVec, m, s.rVec);
      s.rotZ += rZ;
    };
    const rotZVecM = (rZ: number) => {
      const m = BABYLON.Matrix.RotationZ(rZ);
      rotZVec(overall, rZ, m);
      for (const e of sepres) { rotZVec(e, rZ, m); }
    };
    for (const p of pg) {
      const pg = pieceGeo[cp.pieceGeoId][p.pt0WhiteSfen()];
      // これまで並べた駒をbetaの角度で回転
      rotZVecM(l2r ? -pg.beta : pg.beta);
      // ずらして
      addVecM(new BABYLON.Vector3(l2r ? -pg.width - gapWidth : pg.width + gapWidth, 0, 0));
      // 次の持ち駒を配置
      sepres.push(l2r ? {
        lVec: new BABYLON.Vector3(-pg.width - gapWidth, 0, 0),
        cVec: new BABYLON.Vector3(-pg.hwidth - 0.5 * gapWidth, 0, 0),
        rVec: BABYLON.Vector3.Zero(),
        rotZ: 0,
      } : {
        lVec: BABYLON.Vector3.Zero(),
        cVec: new BABYLON.Vector3(pg.hwidth + 0.5 * gapWidth, 0, 0),
        rVec: new BABYLON.Vector3(pg.width + gapWidth, 0, 0),
        rotZ: 0,
      });
      // さらにbetaの角度で回転
      rotZVecM(l2r ? -pg.beta : pg.beta);
      // 端点を再設定
      l2r ? overall.rVec.set(0, 0, 0) : overall.lVec.set(0, 0, 0);
    }
    addVecM(new BABYLON.Vector3(
      -0.5 * (overall.lVec.x + overall.rVec.x),
      -0.5 * (overall.lVec.y + overall.rVec.y),
      -0.5 * (overall.lVec.z + overall.rVec.z),
    ));
    overall.cVec.set(0, 0, 0);
    rotZVecM(-Math.atan2(overall.rVec.y, overall.rVec.x));
    addVecM(new BABYLON.Vector3(
      -0.5 * (
        Math.max.apply(null, sepres.map((v, i, a) => v.rVec.x)) +
        Math.min.apply(null, sepres.map((v, i, a) => v.lVec.x))
      ),
      -Math.min.apply(null, sepres.map((v, i, a) => v.lVec.y)), 0));
    return { overall, sepres,
      width:
        Math.max.apply(null, sepres.map((v, i, a) => v.rVec.x)) -
        Math.min.apply(null, sepres.map((v, i, a) => v.lVec.x)),
    };
  }

  /**
   * 盤面情報からの駒の配置設定
   * @param force マテリアルの強制設定
   */
  setBoard(force?: boolean, mode = 0): void {
    const cp = this.component;
    const pTrans: TransStHash = {};

    // 盤上駒の配置算出
    for (let sq = 0; sq < 81; sq += 1) {
      const p = this.pos.board[sq];
      if (p) {
        this.setPiece(pTrans, p);
      }
    }

    // 持ち駒の配置算出
    switch (mode) {
      case 0:
        // TODO: これを書いた後で駒台の駒を種類別に分割するようにしたので、もう少し賢く試行錯誤するようにする
        const sortOrder = (pt0: shogi.PieceType0) => [6, 5, 4, 3, 1, 0, 2, 7][pt0];
        for (const handSide of [shogi.Color.BLACK, shogi.Color.WHITE]) {
          const l2r: boolean = cp.handL2R[handSide];
          const gapW: number = cp.handGap[handSide];
          // 例えば、黒側の駒台に白側の駒として載せる事も出来る。
          // 王手放置の反則に対して、敵玉を討ち取って駒台に載せる演出用？
          for (const handColor of [shogi.Color.BLACK, shogi.Color.WHITE]) {
            const hlist = this.pos.hand[handSide].reduce((p, c, i, a) => p.concat(c), [])
              .filter((v, i, a) => (v.color() === handColor))
              .sort((a, b) => (sortOrder(a.pt0()) - sortOrder(b.pt0()) || b.pt2() - a.pt2() || a.seq() - b.seq()));
            let hi = 0;
            let hr = 0;
            while (hi < hlist.length) {
              let hlen = 1;
              let hlenTemp = 1;
              let hpos = this.calcHandPiece(hlist.slice(hi, hi + hlen), l2r, gapW);
              // 同種の駒を数える
              while (hi + hlenTemp < hlist.length && (hlist[hi].pt0() === hlist[hi + hlenTemp].pt0())) {
                hlenTemp += 1;
              }
              let hposTemp = this.calcHandPiece(hlist.slice(hi, hi + hlenTemp), l2r, gapW);
              if (hposTemp.width > cp.handWidth) {
                // 同種の駒を1段に揃えられないなら、並べられる数まで分割する
                let hdiv = 2;
                for (;; hdiv += 1) {
                  const hlenDivided = Math.ceil(hlenTemp / hdiv);
                  hposTemp = this.calcHandPiece(hlist.slice(hi, hi + hlenDivided), l2r, gapW);
                  if (hlenDivided <= 1 || hposTemp.width <= cp.handWidth) {
                    hlen = hlenDivided;
                    hlenTemp -= hlenDivided;
                    hpos = hposTemp;
                    break;
                  }
                }
                do {
                  for (let i = 0; i < hlen; i += 1) {
                    this.setPieceHand(
                      pTrans, hlist[hi + i], handSide,
                      new BABYLON.Vector3(0, (hr - 4.5) * cp.ymul, 0), hpos.sepres[i]);
                  }
                  hi += hlen;
                  hr += 1;
                  hdiv -= 1;
                  hlen = Math.ceil(hlenTemp / hdiv);
                  hpos = this.calcHandPiece(hlist.slice(hi, hi + hlen), l2r, gapW);
                  hlenTemp -= hlen;
                } while (hdiv > 1);
              } else {
                do {
                  // 次の種類の駒も同じ段に揃えられるか試す
                  hlen = hlenTemp;
                  hpos = hposTemp;
                  if (hi + hlenTemp >= hlist.length) { break; }
                  while (hlist[hi + hlen].pt0() === hlist[hi + hlenTemp].pt0()) {
                    hlenTemp += 1;
                    if (hi + hlenTemp >= hlist.length) { break; }
                  }
                  hposTemp = this.calcHandPiece(hlist.slice(hi, hi + hlenTemp), l2r, gapW);
                } while (hposTemp.width <= cp.handWidth);
              }
              // 持ち駒1段出力
              for (let i = 0; i < hlen; i += 1) {
                this.setPieceHand(
                  pTrans, hlist[hi + i], handSide, new BABYLON.Vector3(0, (hr - 4.5) * cp.ymul, 0), hpos.sepres[i]);
              }
              hi += hlen;
              hr += 1;
            }
          }
        }
        break;
      case 1:
        for (const handSide of [shogi.Color.BLACK, shogi.Color.WHITE]) {
          const l2r: boolean = cp.handL2R[handSide];
          const gapW: number = cp.handGap[handSide];
          for (const e of [
            { pt0: shogi.PieceType0.p, rPos: new BABYLON.Vector3(0, -4.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.l, rPos: new BABYLON.Vector3(+0.25 * cp.handWidth, -3.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.n, rPos: new BABYLON.Vector3(-0.25 * cp.handWidth, -3.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.s, rPos: new BABYLON.Vector3(+0.25 * cp.handWidth, -2.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.g, rPos: new BABYLON.Vector3(-0.25 * cp.handWidth, -2.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.b, rPos: new BABYLON.Vector3(+0.25 * cp.handWidth, -1.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.r, rPos: new BABYLON.Vector3(-0.25 * cp.handWidth, -1.5 * cp.ymul, 0) },
            { pt0: shogi.PieceType0.k, rPos: new BABYLON.Vector3(0, -0.5 * cp.ymul, 0) },
          ]) {
            const hlist = this.pos.hand[handSide].reduce((p, c, i, a) => p.concat(c), [])
            .filter((v, i, a) => (v.color() === handSide && v.pt0() === e.pt0))
            .sort((a, b) => (b.pt2() - a.pt2() || a.seq() - b.seq()));
            const hPos = this.calcHandPiece(hlist, l2r, gapW);
            for (let i = 0; i < hlist.length; i += 1) {
              this.setPieceHand(pTrans, hlist[i], handSide, e.rPos, hPos.sepres[i]);
            }
          }
        }
        break;
    }

    const pieceList = [];
    const fps = 60;
    const endFrame = Math.max(1, fps * cp.animateDuration * 0.001);
    const q1Frame = endFrame * 0.24;
    const q2Frame = endFrame * 0.49;
    const q3Frame = endFrame * 0.74;
    const q4Frame = endFrame * 0.99;
    // X軸回りで0.5rad(約28.6度)余分に傾ける
    const q3RotX = 0.5;
    // X軸回りで0.2rad(約11.5度)余分に傾ける
    const q4RotX = 0.2;
    for (const pKey of Object.keys(this.pieces)) {
      if (force) {
        this.pieces[pKey].material =
          this.mats[pTrans[pKey].p.color() === shogi.Color.BLACK ? cp.matidBPiece : cp.matidWPiece].get();
      }
      // 存在しないままならアニメーション省略(消し忘れでなければフェードアウト中)
      if (!this.pieceTrans[pKey] && !pTrans[pKey]) { continue; }
      // 変わっていなければアニメーション省略
      if (this.pieceTrans[pKey] && pTrans[pKey] &&
        this.pieceTrans[pKey].p.v === pTrans[pKey].p.v &&
        this.pieceTrans[pKey].pGeoName === pTrans[pKey].pGeoName &&
        this.pieceTrans[pKey].ptx === pTrans[pKey].ptx &&
        this.pieceTrans[pKey].pos.equals(pTrans[pKey].pos) &&
        this.pieceTrans[pKey].rotQ.equals(pTrans[pKey].rotQ)
      ) {
        // 影生成リストに追加
        pieceList.push(this.pieces[pKey]);
        // 処理済みフラグ立て
        pTrans[pKey].flag = true;
        continue;
      }
      // その駒にアニメーションが残っていたら止める
      if (this.animatablePieces[pKey]) {
        // アニメーションを止めるとアニメーション終端まで一気に飛んでしまうので現状を一時保存
        const pos = this.pieces[pKey].position.clone();
        const rotQ = (this.pieces[pKey].rotationQuaternion || BABYLON.Quaternion.Identity()).clone();
        // アニメーション停止・削除
        this.animatablePieces[pKey].stop();
        delete this.animatablePieces[pKey];
        // 改めて現在の位置を書き戻す
        this.pieces[pKey].position = pos;
        this.pieces[pKey].rotationQuaternion = rotQ;
      }
      // 前と同じ駒がある
      if (pTrans[pKey]) {
        const pTr = pTrans[pKey];
        // 駒形状変更時
        if (this.pieceTrans[pKey] &&
          (this.pieceTrans[pKey].pGeoName !== pTr.pGeoName || this.pieceTrans[pKey].ptx !== pTr.ptx)) {
          const pKeyPrev = this.pieces[pKey];
          this.scene.removeMesh(pKeyPrev);
          this.pieces[pKey] = Object.assign(
            new PieceMesh(pieceGeo[cp.pieceGeoId][pTr.ptx], pieceTexUvNorm[pTr.ptx]).applyScene(pKey, this.scene),
            {
              material: pKeyPrev.material,
              position: pKeyPrev.position,
              rotationQuaternion: pKeyPrev.rotationQuaternion,
              visibility: pKeyPrev.visibility,
            },
          );
        }
        const pObj = this.pieces[pKey];
        pieceList.push(pObj);
        pTr.flag = true;
        // 移動距離
        const distP = pObj.position.subtract(pTr.pos).length();
        // 回転角
        const distR = pTr.rotQ.equals(pObj.rotationQuaternion || BABYLON.Quaternion.Identity()) ? 0 :
          2 * Math.acos(BABYLON.Quaternion.Inverse(pObj.rotationQuaternion || BABYLON.Quaternion.Identity())
          .multiply(pTr.rotQ).w);
        if (cp.animateDuration) {
          // 移動距離 1.25, 回転角 1rad(約57.3度)を超える場合は駒を持ち上げる動作
          pObj.animations = (distP <= 1.25 * cp.ymul && distR <= 1) ? [
            animSet(
              BABYLON.Animation.CreateAnimation(
                'position', BABYLON.Animation.ANIMATIONTYPE_VECTOR3, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: pObj.position },
                { frame: q4Frame, value: Object.assign(pTr.pos.clone(), {
                  z: pTr.pos.z * Math.cos(q4RotX) - pTr.pGeo.hfacevert * Math.sin(q4RotX),
                }) },
                { frame: endFrame, value: pTr.pos },
              ],
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'rotationQuaternion', BABYLON.Animation.ANIMATIONTYPE_QUATERNION, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: (pObj.rotationQuaternion || BABYLON.Quaternion.Identity()) },
                { frame: q4Frame, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, pTr.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, pTr.rotX + q4RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, pTr.rotY)),
                },
                { frame: endFrame, value: pTr.rotQ },
              ],
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'visibility', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: pObj.visibility },
                { frame: q1Frame, value: 1 },
                { frame: endFrame, value: 1 },
              ],
            ),
          ] : [
            animSet(
              BABYLON.Animation.CreateAnimation(
                'position', BABYLON.Animation.ANIMATIONTYPE_VECTOR3, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              // 駒を下ろすまでの経路をベジエ曲線で生成
              bezierAnimationKeys(
                pObj.position,
                Object.assign(pObj.position.clone(), { z: -1.4 * (pTr.pGeo.radius + pTr.pGeo.thick) }),
                Object.assign(pTr.pos.clone(), { z: -1.4 * (pTr.pGeo.radius + pTr.pGeo.thick) }),
                Object.assign(pTr.pos.clone(), {
                  z: pTr.pos.z * Math.cos(q3RotX) - pTr.pGeo.hfacevert * Math.sin(q3RotX),
                }),
                0,
                q3Frame,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
                64,
              ).concat(
                [
                  { frame: q4Frame, value: Object.assign(pTr.pos.clone(), {
                    z: pTr.pos.z * Math.cos(q4RotX) - pTr.pGeo.hfacevert * Math.sin(q4RotX),
                  }) },
                  { frame: endFrame, value: pTr.pos },
                ],
              ),
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'rotationQuaternion', BABYLON.Animation.ANIMATIONTYPE_QUATERNION, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: (pObj.rotationQuaternion || BABYLON.Quaternion.Identity()) },
                // 駒を持ち上げる際、ランダムに多少角度を揺らす
                { frame: q1Frame, value: new BABYLON.Quaternion(
                  0.1 * (Math.random() - 0.5), 0.1 * (Math.random() - 0.5), 0.1 * (Math.random() - 0.5), 1,
                ).normalize().multiply(pObj.rotationQuaternion || BABYLON.Quaternion.Identity()) },
                { frame: q3Frame, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, pTr.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, pTr.rotX + q3RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, pTr.rotY)),
                },
                { frame: q4Frame, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, pTr.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, pTr.rotX + q4RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, pTr.rotY)),
                },
                { frame: endFrame, value: pTr.rotQ },
              ],
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'visibility', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: pObj.visibility },
                { frame: q1Frame, value: 1 },
              ],
            ),
          ];
          pObj.animations[0].addEvent(new BABYLON.AnimationEvent(q2Frame, () => {
            // 中間でマテリアル変更
            pObj.material = this.mats[pTr.p.color() === shogi.Color.BLACK ? cp.matidBPiece : cp.matidWPiece].get();
          }));
          this.animatablePieces[pKey] = this.scene.beginAnimation(pObj, 0, endFrame, false, undefined, () => {
            // タイミングの問題でおかしくなる場合もあるので、アニメーション終了時にももう一度マテリアル変更
            pObj.material = this.mats[pTr.p.color() === shogi.Color.BLACK ? cp.matidBPiece : cp.matidWPiece].get();
            // 駒音再生
            if (pTr.p.square() < 81) this.sound.play();
          });
        } else {
          Object.assign(pObj, {
            position: pTr.pos,
            rotationQuaternion: pTr.rotQ,
            visibility: 1,
            material: this.mats[pTr.p.color() === shogi.Color.BLACK ? cp.matidBPiece : cp.matidWPiece].get(),
          });
        }
      } else {
        // fade out
        const pObj = this.pieces[pKey];
        if (cp.animateDuration) {
          pObj.animations = [
            animSet(
              BABYLON.Animation.CreateAnimation(
                'position', BABYLON.Animation.ANIMATIONTYPE_VECTOR3, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: pObj.position },
                { frame: endFrame, value: Object.assign(pObj.position.clone(), { z: -2 * cp.ymul }) },
              ]),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'visibility', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: pObj.visibility },
                { frame: endFrame, value: 0 },
              ]),
          ];
          this.animatablePieces[pKey] = this.scene.beginAnimation(
            pObj, 0, endFrame, false, undefined, () => {
              // アニメーション終了時に既に同じ駒が盤に再配置されている可能性があるので、盤上に同じ駒がないかチェックする。
              if (this.pos.board.every((v, i, a) =>
                (!v || `${v.pt0WhiteSfen()}${v.seq()}` !== pKey) &&
                this.pos.hand.every((v, i, a) => v.every((v, i, a) => v.every((v, i, a) =>
                (!v || `${v.pt0WhiteSfen()}${v.seq()}` !== pKey)))))
              ) {
                // remove mesh
                this.scene.removeMesh(this.pieces[pKey]);
                delete this.pieces[pKey];
                delete this.animatablePieces[pKey];
              }
            },
          );
        } else {
          this.scene.removeMesh(this.pieces[pKey]);
          delete this.pieces[pKey];
          delete this.animatablePieces[pKey];
        }
      }
      this.rerender(endFrame + 5);
    }

    // 駒メッシュ生成
    for (const res of Object.values(pTrans)) {
      if (res.flag) { continue; }
      pieceList.push(this.pieces[res.pid] = Object.assign(
        new PieceMesh(pieceGeo[cp.pieceGeoId][res.ptx], pieceTexUvNorm[res.ptx]).applyScene(res.pid, this.scene),
        {
          material: this.mats[res.p.color() === shogi.Color.BLACK ? cp.matidBPiece : cp.matidWPiece].get(),
          position: res.pos,
          rotationQuaternion: res.rotQ,
          // 不透明度
          visibility: 1,
          // fade in animation
          animations: [
            animSet(
              BABYLON.Animation.CreateAnimation(
                'position', BABYLON.Animation.ANIMATIONTYPE_VECTOR3, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: Object.assign(res.pos.clone(), { z: -2 * cp.ymul }) },
                { frame: q4Frame, value: Object.assign(res.pos.clone(), {
                  z: res.pos.z * Math.cos(q4RotX) - res.pGeo.hfacevert * Math.cos(q4RotX),
                }) },
                { frame: endFrame, value: res.pos },
              ],
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'rotationQuaternion', BABYLON.Animation.ANIMATIONTYPE_QUATERNION, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, res.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, res.rotX + q3RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, res.rotY)),
                },
                { frame: q3Frame, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, res.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, res.rotX + q3RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, res.rotY)),
                },
                { frame: q4Frame, value:
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Z, res.rotZ).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.X, res.rotX + q4RotX)).multiply(
                  BABYLON.Quaternion.RotationAxis(BABYLON.Axis.Y, res.rotY)),
                },
                { frame: endFrame, value: res.rotQ },
              ],
            ),
            animSet(
              BABYLON.Animation.CreateAnimation(
                'visibility', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
                easeSet(new BABYLON.QuadraticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
              ),
              [
                { frame: 0, value: 0 },
                { frame: q3Frame, value: 1 },
              ],
            ),
          ],
          // 複数駒同士で影を落とすようにしてみたいが、駒が落とす影がその駒自身に落ちて全部の駒が影になってしまう。対策が分かるまでコメントアウト
          // receiveShadows: true,
        },
      ));
      if (cp.animateDuration) {
        // fade in のアニメーションが何故かずれる。見苦しいのでとりあえずアニメーションを抑止。
        // this.animatablePieces[res.pid] = this.scene.beginAnimation(this.pieces[res.pid], 0, endFrame);
      }
    }

    // 次回比較用に今回の配置情報を保存
    this.pieceTrans = pTrans;

    // 駒移動元・移動先マスの強調表示
    if (cp.boolSquareEm && this.pos.stateinfo) {
      for (let sq: shogi.Square = 0; sq < shogi.Square.NB; sq += 1) {
        this.square[sq].setEnabled(
          sq === this.pos.stateinfo.move.fromPiece.square() ||
          sq === this.pos.stateinfo.move.toPiece.square(),
        );
        this.square[sq].visibility = (sq === this.pos.stateinfo.move.toPiece.square() ? 1 : 0.5);
      }
    } else {
      for (let sq: shogi.Square = 0; sq < shogi.Square.NB; sq += 1) {
        this.square[sq].setEnabled(false);
      }
    }

    if (this.shadowGenerator) {
      const shadowMap = this.shadowGenerator.getShadowMap();
      if (shadowMap) {
        shadowMap.renderList = pieceList;
      }
    }

    // 局面情報の表示
    this.textBlock.text = cp.boolBoardInfo ? [
      `ply: ${this.pos.ply}`,
      `turn: ${['gray', 'black', 'white', 'error'][this.pos.turn]}`,
      `sfen: ${this.pos.stateinfo && shogi.moveSfenStr(this.pos.stateinfo.move)}`,
      `csa: ${this.pos.stateinfo && shogi.moveCsaStr(this.pos.stateinfo.move)}`,
      `kif: ${this.pos.stateinfo && shogi.moveKifStr(
        this.pos.stateinfo.move,
        this.pos.stateinfo.prev ? this.pos.stateinfo.prev.move.toPiece.square() : undefined)}`,
      `ki2: ${this.pos.stateinfo && shogi.moveKi2Str(
        this.pos.stateinfo.move, this.pos.stateinfo.canMoveBB,
        this.pos.stateinfo.prev ? this.pos.stateinfo.prev.move.toPiece.square() : undefined)}`,
    ].join('\n') : '';
  }
  /**
   * 再描画設定
   * @param frame 再描画を続けるフレーム数
   */
  rerender(frame = 1): void {
    this.rerenderFrame = Math.max(frame, this.rerenderFrame);
  }
  /**
   * アニメーション初期設定
   */
  animate(): void {
    const cp = this.component;
    // カメラに変更があれば再描画時間設定
    this.camera.onViewMatrixChangedObservable.add(() => this.rerender(120));
    // resize event
    window.addEventListener('resize', () => this.bjsengine.resize());
    // pointer event
    this.scene.onPointerObservable.add(
      (eventData: BABYLON.PointerInfo, eventState: BABYLON.EventState) => {
        // console.log({ eventData, eventState, type: eventData.event.type });
        this.rerender();
      },
    );
    // run the render loop
    this.bjsengine.runRenderLoop(() => {
      const canvas = this.canvas;
      // キャンバスサイズに変更があればリサイズ及び再描画
      if (canvas.clientWidth !== this.canvas.width || canvas.clientHeight !== this.canvas.height) {
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        this.bjsengine.resize();
        this.camReset();
      }
      // 再描画不要な時は抑止して負荷軽減
      if (this.rerenderFrame > 0 || this.rerenderFrame < -120) {
        this.rerender(0);
        this.textBlock.fontSize = `${this.canvas.height * 0.04}px`;
        this.textBlock.shadowBlur = Math.max(this.canvas.height * 0.01, 4);
        // 移動駒・捕獲駒の強調表示
        const moveHighlightMesh: {[index: string]: BABYLON.Mesh} = {};
        if (this.pos.stateinfo) {
          for (const pTr of Object.values(this.pieceTrans)) {
            if (
              cp.boolMoveGlow &&
              pTr.p.pt0() === this.pos.stateinfo.move.toPiece.pt0() &&
              pTr.p.seq() === this.pos.stateinfo.move.toPiece.seq() ||
              cp.boolCapGlow &&
              this.pos.stateinfo.capturedPiece &&
              pTr.p.pt0() === this.pos.stateinfo.capturedPiece.pt0() &&
              pTr.p.seq() === this.pos.stateinfo.capturedPiece.seq()
            ) {
              moveHighlightMesh[pTr.pid] = this.pieces[pTr.pid];
            }
          }
        }
        for (const pid of Object.keys(this.moveHighlightMesh)) {
          if (!moveHighlightMesh[pid]) {
            this.highlightLayer.removeMesh(this.moveHighlightMesh[pid]);
            delete this.mouseHighlightMesh[pid];
          }
        }
        const mouseHighlightMesh: {[index: string]: BABYLON.Mesh} = {};
        if (cp.boolMouseOverGlow) {
          const pickResult = this.scene.multiPick(this.scene.pointerX, this.scene.pointerY);
          if (pickResult) {
            for (const pick of pickResult) {
              if (pick.pickedMesh && this.pieceTrans[pick.pickedMesh.name] &&
                this.pieceTrans[pick.pickedMesh.name].p.color() === this.pos.turn) {
                mouseHighlightMesh[pick.pickedMesh.name] = this.pieces[pick.pickedMesh.name];
                break;
              }
            }
          }
        }
        for (const pid of Object.keys(this.mouseHighlightMesh)) {
          if (!mouseHighlightMesh[pid]) {
            this.highlightLayer.removeMesh(this.mouseHighlightMesh[pid]);
            delete this.mouseHighlightMesh[pid];
          }
        }
        for (const pid of Object.keys(moveHighlightMesh)) {
          if (!this.moveHighlightMesh[pid]) {
            this.highlightLayer.addMesh(moveHighlightMesh[pid], cp.glowColor);
          }
        }
        for (const pid of Object.keys(mouseHighlightMesh)) {
          if (!this.mouseHighlightMesh[pid]) {
            this.highlightLayer.addMesh(mouseHighlightMesh[pid], cp.mouseoverGlowColor);
          }
        }
        this.mouseHighlightMesh = mouseHighlightMesh;
        this.moveHighlightMesh = moveHighlightMesh;
        this.scene.render();
      }
      this.rerenderFrame -= 1;
    });
  }
  /**
   * カメラ位置の再調整
   * @param reverse カメラ反転設定
   */
  camReset(reverse: boolean = this.cameraReverse): void {
    const cp = this.component;
    const camTanV = Math.tan(0.4);
    const camTanH = camTanV * Math.max(this.canvas.width, 1) / Math.max(this.canvas.height, 1);
    const fps = 60;
    const endFrame = Math.max(1, fps * cp.animateDuration * 0.001 | 0);
    this.camera.animations = [
      animSet(
        BABYLON.Animation.CreateAnimation(
          'alpha', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
          easeSet(new BABYLON.ElasticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
        ),
        [
          { frame: 0, value: +this.camera.alpha },
          { frame: endFrame, value: (reverse ? +0.5 : -0.5) * Math.PI },
        ],
      ),
      animSet(
        BABYLON.Animation.CreateAnimation(
          'beta', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
          easeSet(new BABYLON.ElasticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
        ),
        [
          { frame: 0, value: +this.camera.beta },
          { frame: endFrame, value: (reverse ? -0.5 : +0.5) * Math.PI },
        ],
      ),
      animSet(
        BABYLON.Animation.CreateAnimation(
          'radius', BABYLON.Animation.ANIMATIONTYPE_FLOAT, fps,
          easeSet(new BABYLON.ElasticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
        ),
        [
          { frame: 0, value: +this.camera.radius },
          { frame: endFrame, value: Math.max(
              (4.8 * cp.ymul) / camTanV,
              (4.5 * cp.xmul + 0.3 * cp.ymul + cp.handWidth) / camTanH,
            ),
          },
        ],
      ),
      animSet(
        BABYLON.Animation.CreateAnimation(
          'target', BABYLON.Animation.ANIMATIONTYPE_VECTOR3, fps,
          easeSet(new BABYLON.ElasticEase(), BABYLON.EasingFunction.EASINGMODE_EASEOUT),
        ),
        [
          { frame: 0, value: this.camera.target.clone() },
          { frame: endFrame, value: BABYLON.Vector3.Zero() },
        ],
      ),
    ];
    this.scene.beginAnimation(this.camera, 0, endFrame);
    this.rerender(endFrame + 1);
  }
  /**
   * カメラ操作コントロールの開始/停止
   * @param f true: 開始, false: 停止
   */
  attachCameraControl(f: boolean): void {
    if (f) {
      this.camera.attachControl(this.canvas, false);
    } else {
      this.camera.detachControl(this.canvas);
    }
  }

  destroy(): void {
    this.bjsengine.dispose();
  }

}

/**
 * ポインタ操作周りの状態
 */
enum PointerState {
  // 駒操作不可
  VIEW,
  // 手番側の駒操作可
  PLAY,
  // PLAY + 駒操作中
  PLAY_PICK,
  // PLAY + 成・不成の問い合わせ
  PLAY_PROMOTE,
  // 盤面編集モード
  EDIT,
  // EDIT + 駒操作中
  EDIT_PICK,
  // EDIT + 成・不成の問い合わせ
  EDIT_PROMOTE,
}

/**
 * 駒寸法データ
 */
interface PieceGeo {
  height: number; width: number; thick: number;
  alpha: number; beta: number; gamma: number;
  hheight: number; hwidth: number; hthick: number; hfacevert: number;
  shol: number; sholw: number; sholh: number;
  side: number; sidew: number; sideh: number;
  hsholt: number; sholt: number;
  hheadt: number; headt: number;
  sholy: number; perpe: number; radius: number;
}

/**
 * 駒寸法計算
 * @param param パラメータ
 */
const pieceGeoCalc = (param: {
  height: number,
  width: number,
  thick: number,
  alpha?: number,
  beta?: number,
  gamma?: number,
}): PieceGeo => {
  /** 高さ */
  const height = param.height;
  /** 幅 */
  const width = param.width;
  /** 厚さ */
  const thick = param.thick;
  /** 駒底面と駒肩面のなす角 */
  const alpha = param.alpha || 18 * Math.PI / 180;
  const sinAlpha = Math.sin(alpha);
  const cosAlpha = Math.cos(alpha);
  const tanAlpha = Math.tan(alpha);
  /** 駒底面の垂線と駒脇面のなす角 */
  const beta = param.beta || 9 * Math.PI / 180;
  const sinBeta = Math.sin(beta);
  const cosBeta = Math.cos(beta);
  const tanBeta = Math.tan(beta);
  /** 駒底面の垂線と駒表面のなす角 */
  const gamma = param.gamma || 5 * Math.PI / 180;
  const sinGamma = Math.sin(gamma);
  const cosGamma = Math.cos(gamma);
  const tanGamma = Math.tan(gamma);
  /** 0.5 * 高さ */
  const hheight = 0.5 * height;
  /** 0.5 * 幅 */
  const hwidth = 0.5 * width;
  /** 0.5 * 厚さ */
  const hthick = 0.5 * thick;
  /** 0.5 * 駒表面縦長さ */
  const hfacevert = hheight / cosGamma;
  /** 肩部長さ */
  const shol = (hwidth - height * tanBeta) / (cosAlpha - sinAlpha * tanBeta);
  /** 肩部幅 */
  const sholw = shol * cosAlpha;
  /** 肩部高さ */
  const sholh = shol * sinAlpha;
  /** 側部長さ */
  const side = (height - hwidth * tanAlpha) / (cosBeta - sinBeta * tanAlpha);
  /** 側部幅 */
  const sidew = side * sinBeta;
  /** 側部高さ */
  const sideh = side * cosBeta;
  /** 0.5 * 肩厚さ */
  const hsholt = hthick - sideh * tanGamma;
  /** 肩厚さ */
  const sholt = 2 * hsholt;
  /** 0.5 * 頭厚さ */
  const hheadt = hthick - height * tanGamma;
  /** 頭厚さ */
  const headt = 2 * hheadt;
  /** 中心～肩の高さ */
  const sholy = hheight - sholh;
  /** 中心～駒表面までの長さ */
  const perpe = hthick * cosGamma - hheight * sinGamma;
  /** 中心から腰までの長さ */
  const radius = Math.sqrt(hwidth * hwidth + hheight * hheight + hthick * hthick);

  return {
    height, width, thick, alpha, beta, gamma,
    hheight, hwidth, hthick, hfacevert,
    shol, sholw, sholh,
    side, sidew, sideh,
    hsholt, sholt, hheadt, headt,
    sholy, perpe, radius,
  };
};
/**
 * 標準駒寸法(盤のマス目の縦の長さを1とする)
 */
const pieceGeo: {[index: string]: { [index: string]: PieceGeo }} = {
  norm: { // 標準形
    k: /* 王 */ pieceGeoCalc({ height: 0.825, width: 0.741, thick: 0.251 }),
    j: /* 玉 */ pieceGeoCalc({ height: 0.825, width: 0.741, thick: 0.251 }),
    r: /* 飛 */ pieceGeoCalc({ height: 0.800, width: 0.715, thick: 0.240 }),
    b: /* 角 */ pieceGeoCalc({ height: 0.800, width: 0.715, thick: 0.240 }),
    g: /* 金 */ pieceGeoCalc({ height: 0.775, width: 0.690, thick: 0.227 }),
    s: /* 銀 */ pieceGeoCalc({ height: 0.775, width: 0.690, thick: 0.227 }),
    n: /* 桂 */ pieceGeoCalc({ height: 0.750, width: 0.659, thick: 0.214 }),
    l: /* 香 */ pieceGeoCalc({ height: 0.725, width: 0.607, thick: 0.207 }),
    p: /* 歩 */ pieceGeoCalc({ height: 0.700, width: 0.581, thick: 0.200 }),
  },
  thin: { // 薄っぺら
    k: /* 王 */ pieceGeoCalc({ height: 0.825, width: 0.741, thick: 0.050, gamma: 0.001 }),
    j: /* 玉 */ pieceGeoCalc({ height: 0.825, width: 0.741, thick: 0.050, gamma: 0.001 }),
    r: /* 飛 */ pieceGeoCalc({ height: 0.800, width: 0.715, thick: 0.050, gamma: 0.001 }),
    b: /* 角 */ pieceGeoCalc({ height: 0.800, width: 0.715, thick: 0.050, gamma: 0.001 }),
    g: /* 金 */ pieceGeoCalc({ height: 0.775, width: 0.690, thick: 0.050, gamma: 0.001 }),
    s: /* 銀 */ pieceGeoCalc({ height: 0.775, width: 0.690, thick: 0.050, gamma: 0.001 }),
    n: /* 桂 */ pieceGeoCalc({ height: 0.750, width: 0.659, thick: 0.050, gamma: 0.001 }),
    l: /* 香 */ pieceGeoCalc({ height: 0.725, width: 0.607, thick: 0.050, gamma: 0.001 }),
    p: /* 歩 */ pieceGeoCalc({ height: 0.700, width: 0.581, thick: 0.050, gamma: 0.001 }),
  },
};

/**
 * テクスチャUV座標の乗加算値
 */
interface UvMulAdd {
  u0m: number; u0a: number; v0m: number; v0a: number;
  u1m: number; u1a: number; v1m: number; v1a: number;
  u2m: number; u2a: number; v2m: number; v2a: number;
}
/**
 * 標準駒テクスチャ座標
 * - 'u??': 垂直座標(左端:0.00,右端:1.00)
 * - 'v??': 水平座標(下端:0.00,上端:1.00)
 * - 'u0?', 'v0?': 表面
 * - 'u1?', 'v1?': 裏面
 * - 'u2?', 'v2?': 側面
 * - 'u?m', 'v?m': 乗算値
 * - 'u?a', 'v?a': 加算値
 */
const pieceTexUvNorm: { [index: string]: UvMulAdd } = {
  k: { // 王
    u0m: 0.250, u0a: 0.000, v0m: 0.250, v0a: 0.750,
    u1m: 0.000, u1a: 0.240, v1m: 0.000, v1a: 0.990,
    u2m: 0.125, u2a: 0.750, v2m: 0.250, v2a: 0.500,
  },
  j: { // 玉
    u0m: 0.250, u0a: 0.250, v0m: 0.250, v0a: 0.750,
    u1m: 0.000, u1a: 0.240, v1m: 0.000, v1a: 0.990,
    u2m: 0.125, u2a: 0.875, v2m: 0.250, v2a: 0.500,
  },
  r: { // 飛
    u0m: 0.250, u0a: 0.500, v0m: 0.250, v0a: 0.750,
    u1m: 0.250, u1a: 0.750, v1m: 0.250, v1a: 0.750,
    u2m: 0.000, u2a: 0.740, v2m: 0.000, v2a: 0.740,
  },
  b: { // 角
    u0m: 0.250, u0a: 0.000, v0m: 0.250, v0a: 0.500,
    u1m: 0.250, u1a: 0.250, v1m: 0.250, v1a: 0.500,
    u2m: 0.000, v2m: 0.000, u2a: 0.240, v2a: 0.740,
  },
  g: { // 金
    u0m: 0.250, u0a: 0.500, v0m: 0.250, v0a: 0.500,
    u1m: 0.000, u1a: 0.750, v1m: 0.000, v1a: 0.500,
    u2m: 0.000, u2a: 0.240, v2m: 0.000, v2a: 0.740,
  },
  s: { // 銀
    u0m: 0.250, u0a: 0.000, v0m: 0.250, v0a: 0.250,
    u1m: 0.250, u1a: 0.250, v1m: 0.250, v1a: 0.250,
    u2m: 0.000, u2a: 0.240, v2m: 0.000, v2a: 0.490,
  },
  n: { // 桂
    u0m: 0.250, u0a: 0.500, v0m: 0.250, v0a: 0.250,
    u1m: 0.250, u1a: 0.750, v1m: 0.250, v1a: 0.250,
    u2m: 0.000, u2a: 0.740, v2m: 0.000, v2a: 0.490,
  },
  l: { // 香
    u0m: 0.250, u0a: 0.000, v0m: 0.250, v0a: 0.000,
    u1m: 0.250, u1a: 0.250, v1m: 0.250, v1a: 0.000,
    u2m: 0.000, u2a: 0.240, v2m: 0.000, v2a: 0.240,
  },
  p: { // 歩
    u0m: 0.250, u0a: 0.500, v0m: 0.250, v0a: 0.000,
    u1m: 0.250, u1a: 0.750, v1m: 0.250, v1a: 0.000,
    u2m: 0.000, u2a: 0.740, v2m: 0.000, v2a: 0.250,
  },
};

/**
 * 駒の形状計算
 */
class PieceMesh {
  private positions: number[];
  private indices: number[];
  private normals: number[] = [];
  private uvs: number[] = [];
  private vertexData: BABYLON.VertexData;

  constructor(param: PieceGeo, uvma: UvMulAdd) {

    this.positions = [
      // 表面
      0, +param.hheight, -param.hheadt,
      +param.sholw, +param.sholy, -param.hsholt,
      -param.sholw, +param.sholy, -param.hsholt,
      +param.hwidth, -param.hheight, -param.hthick,
      -param.hwidth, -param.hheight, -param.hthick,
      // 裏面
      0, +param.hheight, +param.hheadt,
      +param.sholw, +param.sholy, +param.hsholt,
      -param.sholw, +param.sholy, +param.hsholt,
      +param.hwidth, -param.hheight, +param.hthick,
      -param.hwidth, -param.hheight, +param.hthick,
      // 肩面1
      0, +param.hheight, -param.hheadt,
      0, +param.hheight, +param.hheadt,
      +param.sholw, +param.sholy, -param.hsholt,
      +param.sholw, +param.sholy, +param.hsholt,
      // 側面1
      +param.sholw, +param.sholy, -param.hsholt,
      +param.sholw, +param.sholy, +param.hsholt,
      +param.hwidth, -param.hheight, -param.hthick,
      +param.hwidth, -param.hheight, +param.hthick,
      // 底面
      +param.hwidth, -param.hheight, -param.hthick,
      +param.hwidth, -param.hheight, +param.hthick,
      -param.hwidth, -param.hheight, -param.hthick,
      -param.hwidth, -param.hheight, +param.hthick,
      // 側面2
      -param.hwidth, -param.hheight, -param.hthick,
      -param.hwidth, -param.hheight, +param.hthick,
      -param.sholw, +param.sholy, -param.hsholt,
      -param.sholw, +param.sholy, +param.hsholt,
      // 肩面2
      -param.sholw, +param.sholy, -param.hsholt,
      -param.sholw, +param.sholy, +param.hsholt,
      0, +param.hheight, -param.hheadt,
      0, +param.hheight, +param.hheadt,
    ];

    this.indices = [
      // 表面
      0, 2, 1,
      1, 2, 3,
      2, 4, 3,
      // 裏面
      5, 6, 7,
      6, 8, 7,
      7, 8, 9,
      // 肩面
      10, 12, 11,
      11, 12, 13,
      // 側面
      14, 16, 15,
      15, 16, 17,
      // 底面
      18, 20, 19,
      19, 20, 21,
      // 側面
      22, 24, 23,
      23, 24, 25,
      // 肩面
      26, 28, 27,
      27, 28, 29,
    ];

    BABYLON.VertexData.ComputeNormals(this.positions, this.indices, this.normals);

    const v0 = 0.99 - 0.98 * param.sholh / param.height;
    const u0 = 0.50 - 0.49 * param.sholw / param.hwidth;
    const u1 = 0.50 + 0.49 * param.sholw / param.hwidth;

    const uvcalc = (uv: number[], umul: number, uadd: number, vmul: number, vadd: number) =>
      uv.forEach((v, i) => { this.uvs.push(((i & 1) === 0) ? v * umul + uadd : v * vmul + vadd); });

    // 表面
    uvcalc([0.5, 0.99, u1, v0, u0, v0, 0.99, 0.01, 0.01, 0.01], uvma.u0m, uvma.u0a, uvma.v0m, uvma.v0a);
    // 裏面
    uvcalc([0.5, 0.99, u0, v0, u1, v0, 0.01, 0.01, 0.99, 0.01], uvma.u1m, uvma.u1a, uvma.v1m, uvma.v1a);
    // 肩面・側面・底面
    uvcalc(Array.from({ length: 40 }, (v, k) => 0.5), uvma.u2m, uvma.u2a, uvma.v2m, uvma.v2a);

    this.vertexData = Object.assign(new BABYLON.VertexData(), {
      positions: this.positions,
      indices: this.indices,
      normals: this.normals,
      uvs: this.uvs,
    });

  }
  applyScene(id: string, scene: BABYLON.Scene): BABYLON.Mesh {
    const customMesh = new BABYLON.Mesh(id, scene);
    this.vertexData.applyToMesh(customMesh);
    return customMesh;
  }
}

/**
 * 駒配置パラメータ
 */
interface TransSt {
  pid: string;
  ptx: string;
  pGeoName: string;
  pGeo: PieceGeo;
  p: shogi.Piece;
  pos: BABYLON.Vector3;
  rotZ: number;
  rotX: number;
  rotY: number;
  rotQ: BABYLON.Quaternion;
  flag?: boolean;
}

/**
 * 駒配置パラメータコンテナ
 */
interface TransStHash {
  [pid: string]: TransSt;
}

/**
 * 持ち駒配置用セット
 */
interface TransHand {
  lVec: BABYLON.Vector3;
  cVec: BABYLON.Vector3;
  rVec: BABYLON.Vector3;
  rotZ: number;
}

/**
 * テクスチャ画像の読み込み
 */
const texturePath: {[idx: string]: string} = {
  black5h: require('../assets/3dmodel/piece/texture/plain/zopfli_black5h.png'),
  black1k: require('../assets/3dmodel/piece/texture/plain/zopfli_black1k.png'),
  black2k: require('../assets/3dmodel/piece/texture/plain/zopfli_black2k.png'),
  black4k: require('../assets/3dmodel/piece/texture/plain/zopfli_black4k.png'),
  dark5h:  require('../assets/3dmodel/piece/texture/plain/zopfli_dark5h.png'),
  dark1k:  require('../assets/3dmodel/piece/texture/plain/zopfli_dark1k.png'),
  dark2k:  require('../assets/3dmodel/piece/texture/plain/zopfli_dark2k.png'),
  dark4k:  require('../assets/3dmodel/piece/texture/plain/zopfli_dark4k.png'),
  white5h: require('../assets/3dmodel/piece/texture/plain/zopfli_white5h.png'),
  white1k: require('../assets/3dmodel/piece/texture/plain/zopfli_white1k.png'),
  white2k: require('../assets/3dmodel/piece/texture/plain/zopfli_white2k.png'),
  white4k: require('../assets/3dmodel/piece/texture/plain/zopfli_white4k.png'),
  wood5h:  require('../assets/3dmodel/piece/texture/plain/zopfli_wood5h.png'),
  wood1k:  require('../assets/3dmodel/piece/texture/plain/zopfli_wood1k.png'),
  wood2k:  require('../assets/3dmodel/piece/texture/plain/zopfli_wood2k.png'),
  wood4k:  require('../assets/3dmodel/piece/texture/plain/zopfli_wood4k.png'),
};

/**
 * サウンド読み込み
 */
const soundPath: {[idx: string]: string} = {
  none: '',
  battle07: require('../assets/effect/sound/maoudamashii/se_maoudamashii_battle07.ogg'),
  battle08: require('../assets/effect/sound/maoudamashii/se_maoudamashii_battle08.ogg'),
  battle11: require('../assets/effect/sound/maoudamashii/se_maoudamashii_battle11.ogg'),
  battle15: require('../assets/effect/sound/maoudamashii/se_maoudamashii_battle15.ogg'),
  battle16: require('../assets/effect/sound/maoudamashii/se_maoudamashii_battle16.ogg'),
  finger01: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_finger01.ogg'),
  ignition01: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_ignition01.ogg'),
  ignition02: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_ignition02.ogg'),
  ignition04: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_ignition04.ogg'),
  pc01: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_pc01.ogg'),
  pc03: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_pc03.ogg'),
  sound05: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_sound05.ogg'),
  sound16: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_sound16.ogg'),
  sound19: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_sound19.ogg'),
  sound22: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_sound22.ogg'),
  switch01: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_switch01.ogg'),
  switch02: require('../assets/effect/sound/maoudamashii/se_maoudamashii_se_switch02.ogg'),
};

/**
 * アニメーション構築
 */
const animSet = (
  anim: BABYLON.Animation,
  keys: BABYLON.IAnimationKey[],
): BABYLON.Animation => {
  anim.setKeys(keys);
  return anim;
};

/**
 * アニメーション補間構築
 */
const easeSet = (
  easingFunc: BABYLON.EasingFunction,
  easingMode: number,
): BABYLON.EasingFunction => {
  easingFunc.setEasingMode(easingMode);
  return easingFunc;
};

/**
 * 3次ベジエ補間
 * @param v0
 * @param v1
 * @param v2
 * @param v3
 * @param t
 */
const bezierVec3 = (
  v0: BABYLON.Vector3,
  v1: BABYLON.Vector3,
  v2: BABYLON.Vector3,
  v3: BABYLON.Vector3,
  t: number,
): BABYLON.Vector3 => {
  const u = 1 - t;
  const u2 = u * u;
  const t2 = t * t;
  return v0.scale(u * u2)
  .addInPlace(v1.scale(3 * t * u2))
  .addInPlace(v2.scale(3 * u * t2))
  .addInPlace(v3.scale(t * t2));
};

/**
 * 3次ベジエ曲線による補間アニメーション
 * @param v0
 * @param v1
 * @param v2
 * @param v3
 * @param beginFrame
 * @param endFrame
 * @param easingFunc
 * @param divide
 */
const bezierAnimationKeys = (
  v0: BABYLON.Vector3,
  v1: BABYLON.Vector3,
  v2: BABYLON.Vector3,
  v3: BABYLON.Vector3,
  beginFrame: number,
  endFrame: number,
  easingFunc: BABYLON.EasingFunction,
  divide: number,
): BABYLON.IAnimationKey[] => {
  const res: BABYLON.IAnimationKey[] = [];
  const intFrame = endFrame - beginFrame;
  const c = 1 / divide;
  for (let i = 0; i < divide; i += 1) {
    const t = i * c;
    res.push({
      frame: beginFrame + t * intFrame,
      value: bezierVec3(v0, v1, v2, v3, easingFunc.ease(t)),
    });
  }
  res.push({ frame: endFrame, value: v3 });
  return res;
};
