import { RouteConfig } from 'vue-router';
import homeVue from './home.vue';
import multiView3dVue from './multi-view3d.vue';
import render2dVue from './render2d.vue';
import sandView3dVue from './sand-view3d.vue';
import shaderTestVue from './shader-test.vue';
import singleView3dVue from './single-view3d.vue';
import webglReportVue from './webgl-report.vue';
import ttsTestVue from './tts-test.vue';

export const routeConfList: RouteConfig[] = [
  { path: '/', name: 'home', component: homeVue },
  { path: '/sand-view3d', name: 'sandView3d', component: sandView3dVue },
  { path: '/single-view3d', name: 'singleView3d', component: singleView3dVue },
  { path: '/multi-view3d', name: 'multiView3d', component: multiView3dVue },
  { path: '/render2d', name: 'render2d', component: render2dVue },
  { path: '/shader-test', name: 'shaderTest', component: shaderTestVue },
  { path: '/webgl-report-external-link', name: 'webglReport', component: webglReportVue },
  { path: '/tts-test', name: 'ttsTest', component: ttsTestVue },
];
