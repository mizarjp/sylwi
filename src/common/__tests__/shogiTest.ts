import 'jest';
import * as shogi from '../shogi';

describe('position', () => {
  it('startpos', () => {
    const pos = new shogi.Position();
    pos.set('startpos');
    expect(pos.sfen()).toBe('lnsgkgsnl/1r5b1/ppppppppp/9/9/9/PPPPPPPPP/1B5R1/LNSGKGSNL b - 1');
  });
  it('moves', () => {
    const pos = new shogi.Position();
    // tslint:disable-next-line:max-line-length
    pos.set('position startpos moves 2g2f 3c3d 7g7f 8c8d 2f2e 8d8e 6i7h 4a3b 2e2d 2c2d 2h2d 8e8f 8g8f 8b8f 2d3d 2b3c 5i5h 5a6b 3d3f 8f8d 4i3h 9c9d 9g9f 7a7b 3i4h 8d2d P*2h 2d8d 8i7g 6b5b 3f2f 3a2b 2f5f 3c4b 7f7e 5b4a P*8e 8d2d 3g3f 2b3c 2i3g 3c4d 8e8d P*8b 4g4f 2a3c 4h4g 6c6d 5f7f 5c5d 7i6h 1c1d 5g5f P*3e 7g8e 3e3f 4g3f 2d3d 3h2g P*3e 3f4g 6d6e 6h5g 4a5b 7h6h 6a6b 7f7i 3b2c 4f4e 4d5c 7e7d 7c7d 5g4f 5d5e 8h5e 5c6d 5e8h 7d7e 4e4d 4c4d P*3f 3e3f 2g3f 3d2d 7i2i P*3d P*3e 5b6c 3e3d 2c3d P*4c 4b5c P*3e 3d2e 3g2e 3c2e 4c4b+ 5c4b 3e3d 6e6f 8h6f N*5d 6f5g 8a7c 4f5e 6d5e 5f5e 7c6e 5e5d 6e5g+ 6h5g 7e7f G*6e 2e1g 1i1g B*1h S*4c 7b6a 4c4b+ 1h2i+ B*8a');
    expect(pos.sfen()).toBe('lB1s4l/1p1g1+S3/3k5/pP2PpPrp/1N1G5/P1p3G2/3PGS2L/4K2P1/L6+b1 w 3N2Prs5p 122');
  });
});
