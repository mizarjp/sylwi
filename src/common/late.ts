// 遅延初期化
export default class late<T> {
  private value: T | null;
  private generator: () => T;
  constructor(generator: () => T) {
    this.generator = generator;
    this.value = null;
  }
  get(): T {
    if (this.value === null) {
      this.value = this.generator();
    }
    return this.value;
  }
  clear(): late<T> {
    this.value = null;
    return this;
  }
}
