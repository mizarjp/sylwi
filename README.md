# Sylwi

> Sylwi is a Shogi (japanese-chess-game) GUI, in a very early stage in development.

## About naming "Sylwi"

> In Welsh, "sylwi" seems to have meaning like "notice" or "observe".
> See: https://translate.google.com/#cy/en/sylwi

## Getting Started
Simply clone down this reposity, install dependencies, and get started on this application.

The use of the [yarn](https://yarnpkg.com/) package manager is **strongly** recommended, as opposed to using `npm`.

```bash
# create a directory of your choice, and copy using curl
mkdir sylwi && cd sylwi
curl -fsSL https://github.com/mizar/sylwi/archive/master.tar.gz | tar -xz --strip-components 1

# or copy using git clone
git clone https://github.com/mizar/sylwi.git
cd sylwi

# install dependencies
yarn
```

### Development Scripts

```bash
# run application in development mode
yarn dev

# compile source code and create webpack output
yarn compile

# `yarn compile` & create build with electron-builder
yarn dist

# `yarn compile` & create unpacked build with electron-builder
yarn dist:dir

# update dependencies
## install global packages
yarn global add lerna npm-check-updates
## view global bin path (may need set the path enviroment)
yarn global bin
## mainpackage update dependencies
npm-check-updates -a
## subpackage update dependencies
yarn update-deps
## install dependencies
yarn
```
